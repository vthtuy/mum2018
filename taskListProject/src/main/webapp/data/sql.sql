DROP TABLE IF EXISTS category;
CREATE TABLE IF NOT EXISTS category(
  id integer PRIMARY KEY,
  name varchar
);

DROP TABLE IF EXISTS team;
CREATE TABLE IF NOT EXISTS team(
  id varchar PRIMARY KEY,
  name varchar,
  note varchar
);

DROP TABLE IF EXISTS user;
CREATE TABLE IF NOT EXISTS user(
  id INTEGER PRIMARY KEY,
  email varchar,
  teamId INTEGER,
  password varchar,
  name varchar,
  bio varchar,
  avatar varchar,
  phone varchar,
  location varchar ,
  address varchar,
  lat varchar ,
  long varchar
);
DROP TABLE IF EXISTS task;
CREATE TABLE IF NOT EXISTS task(
  id integer PRIMARY KEY,
  categoryId integer ,
  teamId integer ,
  assignee integer,
  name varchar ,
  dueDate date   ,
  status varchar DEFAULT 'Created',
  priority integer DEFAULT 1,
  description varchar ,
  createdDate date ,
  updatedDate date,
  createdBy date
);
DROP TABLE IF EXISTS tasklog;
CREATE TABLE IF NOT EXISTS tasklog(
  id integer PRIMARY KEY,
  taskId integer,
  userId integer ,
	hour integer ,
	note varchar ,
  workDate date,
	createDate date
);


