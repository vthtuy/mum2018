 -- role admin=1; role employee=2; both=3
INSERT INTO category(id,  name  ) VALUES
(1, "Personal"),
(2, "Company"),
(3, "Frontend - Layout"),
(4, "Java"),
(5, "System"),
(6, "Research")
;

INSERT INTO team(id,  name  ) VALUES
(1, "Administrator"),
(2, "Backend Developer"),
(3, "Frontend Developer"),
(4, "Management"),
(5, "Solution Architector"),
(6, "Leader")
;

INSERT INTO user(
  id, email , teamId , password , name , bio , avatar , phone , location  , address , lat  , long) VALUES
(1, "Administrator@gmail.com", 1, '123', "Administrator", 'a bc', 'avatar', '123456789', 'Fairfield', 'address', '-25.344', '131.036'),
(2, "Backend@gmail.com", 2, '123', "Backend Developer", 'a bc', 'avatar', '123456789', 'Iowa', 'address', '-25.344', '131.036'),
(3, "Frontend@gmail.com", 3, '123', "Frontend Developer", 'a bc', 'avatar', '123456789', '1000 North, 4th, Fairfield', 'address', '-25.344', '131.036'),
(4, "Management@gmail.com", 4, '123', "Management", 'a bc', 'avatar', '123456789', 'Ottumwa', 'address', '-25.344', '131.036'),
(5, "Solution@gmail.com", 5, '123', "Solution Architector", 'a bc', 'avatar', '123456789', 'Iowa', 'address', '-25.344', '131.036'),
(6, "Leader@gmail.com", 6, '123', "Leader", 'a bc', 'avatar', '123456789', 'Iowa', 'address', '-29.344', '131.036'),
(7, "test@gmail.com", 6, '123', "test", 'a bc', 'avatar', '123456789', 'Iowa', 'address', '-29.344', '131.036')
;

INSERT INTO task(
id, categoryId, name, teamId, assignee, priority) VALUES
--id, categoryId , name , teamId  , assignee   ,  dueDate  , status  , priority  , description  ,  createdDate  , updatedDate , createdBy) VALUES
(1, 3, "Task Management team 1", 1, 1, 1),
(2, 2, "User Management team 2 ", 2, 2, 2),
(3, 5, "DB", 2, 3, 1),
(4, 3, "Frontend team 1", 1, 4, 1),
(5, 2, "Login page", 2, 5, 2),
(9, 5, "Checkout page", 3, 6, 3),
(8, 5, "Payment page", 1, 1, 3),
(7, 5, "Order page", 3, 2, 3),
(10, 5, "Order History page", 3, 3, 3)
;
