<%--
  Created by IntelliJ IDEA.
  User: TuyVo
  Date: 2/4/2019
  Time: 11:54 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Task - Init</title>
</head>
<body>
<form method="post">
    <h2>Initial data </h2>

    <p>
        Init result: ${result}. Try again if you got any error :)
    </p>

    <input type="submit" value="Init">
</form>
</body>
</html>
