<!DOCTYPE html>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Task list</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" type="image/x-icon" media="all" href="resources/images/favicon.ico"/>

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round|Open+Sans">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="resources/styles/tasks.css"	media="screen" />
    <link rel="stylesheet" type="text/css" href="resources/styles/jquery-ui.structure.css"	media="screen" />
    <link rel="stylesheet" type="text/css" href="resources/styles/jquery-ui.theme.css"	media="screen" />
    <script src="resources/scripts/jquery-2.0.3.js"></script>
    <script src="resources/scripts/jquery-tmpl.js"></script>
    <script src="resources/scripts/jquery.validate.js"></script>
    <script src="resources/scripts/jquery-serialization.js"></script>
    <script src="resources/scripts/tasks-controller.js"></script>
    <script src="resources/scripts/date.js"></script>
    <script src="resources/scripts/jquery-ui.js"></script>
    <link rel="stylesheet" type="text/css" href="resources/styles/nav.css">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <style>
        .btn {
            font-size: smaller;
        }
        select {
            font-size: smaller;
        }
    </style>
</head>
<body>
<div class="container">

    <div class="row">
        <div class="col-sm">
            <nav id="navigator" class="navbar navbar-icon-top fixed-top navbar-expand-lg navbar-light" style="background-color: #4fb9bc;">
                <a class="navbar-brand" href="#">
                    <img src="resources/images/logo.png" width="70" height="70" alt="">
                    TaskList
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item active">
                            <a class="nav-link" href="home">
                                <i class="fa fa-tasks"></i>
                                Tasks
                                <span class="sr-only">(current)</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="user">
                                <i class="fa fa-users">
                                </i>
                                Users
                            </a>
                        </li>
                    </ul>

                </div>
            </nav>
        </div>
    </div>
    <div class="row">
        <div class="col-sm">
            <br><br>
            <div class="table-wrapper">
                <main  id="taskPage" >
                    <%--<nav>--%>
                    <button id="btnAddTask" type="button" class="btn btn-info" data-toggle="modal" data-target="#exampleModal" >Add</button>
                    <%--<a href="#" id="btnAddTask">Add task</a>--%>
                    <button class="btn btn-info"  id="btnRetrieveTasks">Retrieve all</button>


                    <label class="filterLabel">Filter by:</label>
                    <span>
						<label class="filterLabel">Priority</label> <select name="priority" id="filterPriority">
						<option value="1">High</option>
						<option value="2">Medium</option>
						<option value="3">Low</option>
					</select>
					</span>
                    <span>
						<label class="filterLabel">Sort by</label> <select name="sortBy" id="sortBy">
							<option value="Priority-ASC">Priority-ASC</option>
							<option value="Priority-DESC">Priority-DESC</option>
						</select>
					</span>

                    <section id="taskCreation" ><%--class="not"--%>
                        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Task</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <form id="taskForm">
                                            <input type="hidden" name="id"/>
                                            <input type="hidden" name="status"/>
                                            <div class="form-group">
                                                <label for="taskName" class="col-form-label">Task</label>
                                                <input id="taskName" type="text" required="required"
                                                       name="name" class="large" placeholder="Breakfast at Tiffanys" maxlength="200"  class="form-control"/>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-form-label">Priority</label> <select name="priority" class="form-control">
                                                <option value="1">High</option>
                                                <option value="2">Medium</option>
                                                <option value="3">Low</option>
                                            </select>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-form-label">Assignee</label>  <input type="text" name="assigneeEmail" class="form-control"/>
                                                <a href="#" id="changeAssignee">Change Assignee</a>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-form-label">Required by</label> <input type="date" required="required"
                                                                                                         name="dueDate" class="form-control"/>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-form-label">Category</label> <select name="categoryId" class="form-control">
                                                <option value="1">Personal</option>
                                                <option value="2">Work</option>
                                                <option value="3">Frontend - Layout</option>
                                                <option value="4">Java</option>
                                                <option value="5">System</option>
                                                <option value="6">Research</option>
                                            </select>
                                            </div>
                                            <%--<nav>
                                                <a href="#" id="saveTask">Save task</a>    <!-- https://stackoverflow.com/questions/4855168/what-is-href-and-why-is-it-used -->
                                                <a href="#" id="clearTask">Clear task</a>
                                            </nav>--%>
                                        </form>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" id="clearTask" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        <button type="button"  id="saveTask" class="btn btn-primary">Save task</button>
                                    </div>
                                </div>
                            </div>


                        </div>

                    </section>
                    <section>



                    </section>
                    <section>
                        <table id="tblTasks" >
                            <colgroup>
                                <col width="30%">
                                <col width="10%">
                                <col width="5%">
                                <col width="15%">
                                <col width="15%">
                                <col width="10%">
                                <col width="15%">
                            </colgroup>
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Status</th>
                                <th>Priority</th>
                                <th>Assignee</th>
                                <th>Due Date</th>
                                <th>Category</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>

                    </section>

                    <nav>
                        <div class="ui-category-widget" style="display: inline-block;">
                            <select id="categoryBox">
                                <option value="">-- All Team --</option>
                                <c:forEach var="item" items="${listTeam}">
                                    <option value="${item.id}">${item.name}</option>
                                </c:forEach>
                            </select>
                        </div>
                        <button class="btn btn-info" id="btnRetrieveTasksByTeam">Retrieve by Team</button>

                        <div class="ui-category-widget" style="display: inline-block;">
                            <select id="userCombo">
                                <option value="">-- All User --</option>
                                <c:forEach var="item" items="${listUser}">
                                    <option value="${item.id}">${item.name}</option>
                                </c:forEach>
                            </select>
                        </div>
                        <button class="btn btn-info" id="btnRetrieveTasksByUser">Retrieve by User</button>
                    </nav>
                </main>

            </div>

            <footer>You have <span id="taskCount"></span> tasks</footer>
        </div>
    </div>
</div>


<!-- HTML -->
<div id = "dialog-4" title = "Take a note of task" style="display: none">
    <input id="txtTaskNote" style="width: 90%;" pattern="\w+"/>
</div>
</body>
<script>
    function initScreen() {
        $(document).ready(function() {
            tasksController.init($('#taskPage'), function() {
                tasksController.loadTasks();
                tasksController.retrieveTasksServerLoadPage();
            });

        });
    }
    if (window.indexedDB) {
        console.log("using indexedDB 111917kl");
        $.getScript( "resources/scripts/tasks-indexeddb.js" )
            .done(function( script, textStatus ) {
                initScreen();
            })
            .fail(function( jqxhr, settings, exception ) {
                console.log( 'Failed to load indexed db script' );
            });
    } else if (window.localStorage) {
        console.log("using webstorage 111917kl");
        $.getScript( "resources/scripts/tasks-webstorage.js" )
            .done(function( script, textStatus ) {
                initScreen();
            })
            .fail(function( jqxhr, settings, exception ) {
                console.log( 'Failed to load web storage script' );
            });
    }

</script>

<!-- template put in jsp file has the same syntax with EL , it makes wrong template -->
<jsp:include page="template.html" />


</html>

