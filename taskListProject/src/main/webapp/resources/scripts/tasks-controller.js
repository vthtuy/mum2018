tasksController = function() {

	function errorLogger(errorCode, errorMessage) {
		console.log(errorCode +':'+ errorMessage);
	}

	var taskPage;
	var initialised = false;
	var mode ="add";
	var allTasks;
	var filteredTask;
	// retrieveTasksServer();
	/**
	 * makes json call to server to get task list.
	 * currently just testing this and writing return value out to console
	 * 111917kl
	 */
	function retrieveTasksServer() {
		$.ajax("TaskServlet", {
			"type": "get",
			dataType: "json"
			// "data": {
			//     "first": first,
			//     "last": last
			// }
		}).done(displayTasksServer.bind()); //need reference to the tasksController object
	}
	/** @author tuy.vo **/
	function retrieveTasksServerByTeam() {
		$.ajax("TaskOfTeamServlet", {
				"type" : "get",
				"dataType": "json",
				"data" : {
					"teamId" : $("#categoryBox").val()
				}
			}
		).done(displayTasksServerByTeam.bind()) ;
	}

	/** @author cong.nguyen **/
	function retrieveTasksServerByUser() {
		$.ajax("taskuser", {
				"type" : "get",
				"dataType": "json"
			}
		).done(displayTasksServerByUser.bind()) ;
	}

	function saveTaskNote(taskId) {
		$.ajax("TaskOfTeamServlet", {
				"type" : "post",
				"dataType" : "json",
				"data" : {
					"taskId" : taskId,
					"note" : $("#txtTaskNote").val()
				}
			}
		).done(saveTaskNoteSuccessfully);
	}

	function saveTaskNoteSuccessfully() {
		console.log("Save note fail !");
	}

	function displayTasksServerByTeam(data) { //this needs to be bound to the tasksController -- used bind in retrieveTasksServer 111917kl
		console.log("aaaaaaaa displayTasksServerByTeam ===");
		if ($("#categoryBox").val()) {
			tasksController.loadServerTasksByTeam(data);
		}
		else {
			// all team & sort by teamId
			tasksController.loadServerTasksAllTeam(data);
		}
	}

//congnguyen
	function displayTasksServerByUser(data) { //this needs to be bound to the tasksController -- used bind in retrieveTasksServer 111917kl
		console.log(" displayTasksServerByUser  ===");
		if ($("#userCombo").val()) {
			console.log(" show sort  ===");
			// all team & sort by teamId
			tasksController.loadServerTasksByUser(data);
		} else {
			tasksController.loadServerTasksAllUser(data);
		}
	}
	/**
	 * 111917kl
	 * callback for retrieveTasksServer
	 * @param data
	 */
	function displayTasksServer(data) { //this needs to be bound to the tasksController -- used bind in retrieveTasksServer 111917kl
		console.log(data);
		tasksController.loadServerTasks(data);
	}

	function taskCountChanged() {
		var count = $(taskPage).find( '#tblTasks tbody tr').length;
		$('footer').find('#taskCount').text(count);
	}

	function clearTask() {
		$(taskPage).find('form').fromObject({});
	}

	function renderTable() {
		$.each($(taskPage).find('#tblTasks tbody tr'), function(idx, row) {
			var due = Date.parse($(row).find('[datetime]').text());
			if (due.compareTo(Date.today()) < 0) {
				$(row).addClass("overdue");
			} else if (due.compareTo((2).days().fromNow()) <= 0) {
				$(row).addClass("warning");
			}
		});
	}

	function showTaskNoteDialog(taskId) {
		$( "#dialog-4" ).dialog({
			autoOpen: false,
			modal: true,
			show: {
				effect: "blind",
				duration: 1000
			},
			hide: {
				effect: "explode",
				duration: 1000
			},
			buttons: {
				OK: function() {$(this).dialog("close");
					saveTaskNote(taskId);
				}
			},
		});
	}

	return {
		init : function(page, callback) {
			if (initialised) {
				callback()
			} else {
				taskPage = page;
				storageEngine.init(function() {
					storageEngine.initObjectStore('task', function() {
						callback();
					}, errorLogger)
				}, errorLogger);
				$(taskPage).find('[required="required"]').prev('label').append( '<span>*</span>').children( 'span').addClass('required');
				$(taskPage).find('tbody tr:even').addClass('even');

				$(taskPage).find('#btnAddTask').click(function(evt) {
					console.log('making ajax call btnRetrieveTasksByTeam');
					evt.preventDefault();
					mode ="add";
					$(taskPage).find('#taskCreation').removeClass('not');
				});

				/**	 * 11/19/17kl        */
				$(taskPage).find('#btnRetrieveTasks').click(function(evt) {
					evt.preventDefault();
					console.log('making ajax call');
					retrieveTasksServer();
				});

				/**	 * load task by team
				 * @author tuy.vo        */
				$(taskPage).find('#btnRetrieveTasksByTeam').click(function(evt) {

					evt.preventDefault();
					console.log('making ajax call btnRetrieveTasksByTeam');
					retrieveTasksServerByTeam();
				});

				/**	 * load task by user
				 * @author cong.nguyen        */
				$(taskPage).find('#btnRetrieveTasksByUser').click(function(evt) {

					evt.preventDefault();
					console.log('making ajax call btnRetrieveTasksByUser');
					retrieveTasksServerByUser();
				});

				$(taskPage).find('#tblTasks tbody').on('click', 'tr', function(evt) {
					$(evt.target).closest('td').siblings().andSelf().toggleClass('rowHighlight');
				});

				$(taskPage).find('#tblTasks tbody').on('click', '.deleteRow',
					function(evt) {
						/*storageEngine.delete('task', $(evt.target).data().taskId,
							function() {
								$(evt.target).parents('tr').remove();
								taskCountChanged();
							}, errorLogger);*/
						console.log("delete");
						console.log($(evt.target).data());
						$.post("TaskServlet", {"action":"delete", "id" : $(evt.target).data().taskId})
							.done(retrieveTasksServer);

					}
				);

				$(taskPage).find('#tblTasks tbody').on('click', '.editRow',
					function(evt) {
						$(taskPage).find('#taskCreation').removeClass('not');
						/*storageEngine.findById('task', $(evt.target).data().taskId, function(task) {
							$(taskPage).find('form').fromObject(task);
						}, errorLogger);*/
						mode ="update";
						console.log($(evt.target).data());
						$(taskPage).find('form').fromObject($(evt.target).data());
					}
				);

				$(taskPage).find('#tblTasks tbody').on('click', '.noteRow',
					function(evt) {
						console.log($(evt.target));
						//$(evt.target).data().taskId; // data-task-id ==> data().taskId
						$("#txtTaskNote").val('');
						showTaskNoteDialog($(evt.target).data().taskId);
						var theDialog = $("#dialog-4").dialog( );
						theDialog.dialog("open");

					}
				);

				$(taskPage).find('#clearTask').click(function(evt) {
					evt.preventDefault();
					clearTask();
				});

				$(taskPage).find('#tblTasks tbody').on('click', '.completeRow', function(evt) {
					/*storageEngine.findById('task', $(evt.target).data().taskId, function(task) {
						task.complete = true;
						storageEngine.save('task', task, function() {
							tasksController.loadTasks();
						},errorLogger);
					}, errorLogger);*/
					if ($(taskPage).find('form').valid()) {
						var task = {"id":$(evt.target).data().taskId, "status": "Complete"};
						task.action="changeStatus";
						$.post("TaskServlet", task).done(retrieveTasksServer);

					}
				});

				/*$(taskPage).find('#saveTask').click(function(evt) {

					evt.preventDefault();
					if ($(taskPage).find('form').valid()) {
						var task = $(taskPage).find('form').toObject();
						storageEngine.save('task', task, function() {
							$(taskPage).find('#tblTasks tbody').empty();
							tasksController.loadTasks();
							clearTask();
							$(taskPage).find('#taskCreation').addClass('not');
						}, errorLogger);
					}
				});*/

				$(taskPage).find('#saveTask').click(function(evt) {
					evt.preventDefault();
					if ($(taskPage).find('form').valid()) {
						var task = $(taskPage).find('form').toObject();
						task.action = mode;
						console.log("before save --");
						$.post("TaskServlet", task).done(retrieveTasksServer);
						$('#exampleModal').modal('hide');
					}
				});

				$(taskPage).find('#filterPriority').change(function(evt) {
					evt.preventDefault();
					$(taskPage).find('#tblTasks tbody').empty();
					$.each(allTasks, function (index, task) {
						if($(evt.target).val() == task.priority) {
							$('#taskRow').tmpl(task).appendTo($(taskPage).find('#tblTasks tbody'));
						}
						//renderTable(); --skip for now, this just sets style class for overdue tasks 111917kl
					});
					taskCountChanged();
				});

				$(taskPage).find('#sortBy').change(function(evt) {
					evt.preventDefault();
					$(taskPage).find('#tblTasks tbody').empty();

					var sortFunc = function (a, b){
						return a.priority - b.priority;
					};

					if($(evt.target).val() ==="Priority-DESC") {
						sortFunc = function (a, b){
							return b.priority - a.priority;
						};
					}
					allTasks.sort(sortFunc);

					$.each(allTasks, function (index, task) {
						$('#taskRow').tmpl(task).appendTo($(taskPage).find('#tblTasks tbody'));
						//renderTable(); --skip for now, this just sets style class for overdue tasks 111917kl
					});
				});
				initialised = true;
			}
		},
		/**
		 * 111917kl
		 * modification of the loadTasks method to load tasks retrieved from the server
		 */
		loadServerTasks: function(tasks) {
			allTasks = tasks;
			$(taskPage).find('#tblTasks tbody').empty();
			$.each(tasks, function (index, task) {
				if (!task.complete) {
					task.complete = false;
				}
				$('#taskRow').tmpl(task).appendTo($(taskPage).find('#tblTasks tbody'));
				taskCountChanged();
				console.log('about to render table with server tasks loadServerTasks');
				//renderTable(); --skip for now, this just sets style class for overdue tasks 111917kl
			});
		},

		/**
		 * @author tuy.vo
		 * @param tasks
		 */
		loadServerTasksByTeam: function(teams) {

			$(taskPage).find('#tblTasks tbody').empty();
			let team = teams[0]; // TODO the first team
			for (let i = 1; i < teams.length; i++) {
				if (teams[i].id == $("#categoryBox").val() ) {
					team = teams[i];
				}
			}
			//$.each(teams, function (index, team) {

			let tasks = team.taskList;
			let tasksSort = tasks;
			tasksSort.sort(function (a, b) {
				// sort by name
				return a.name.localeCompare(b.name);
				//return a.id < b.id;
			});

			$.each(tasksSort, function( indexTask, task) {
				console.log("ttt: " + task.id);
				console.log("ttt: " + task.name);
				if (!task.complete) {
					task.complete = false;
				}

				$('#taskRow').tmpl(task).appendTo($(taskPage).find('#tblTasks tbody'));
				taskCountChanged();
				console.log('about to render table with server tasks loadServerTasksByTeam');
				//renderTable(); --skip for now, this just sets style class for overdue tasks 111917kl
			})

			//});
		},
		loadServerTasksAllTeam: function(teams) {

			$(taskPage).find('#tblTasks tbody').empty();
			let teamSort = teams;
			teamSort.sort(function (a, b) {
				// sort by name
				// return a.name.localeCompare(b.name);
				return a.id < b.id;
			});
			// teamSort.sort();
			$.each(teamSort, function (index, team) {

				let tasks = team.taskList;
				$.each(tasks, function( indexTask, task) {
					console.log("team id: " + team.id);
					console.log("task.name: " + task.name);
					if (!task.complete) {
						task.complete = false;
					}

					$('#taskRow').tmpl(task).appendTo($(taskPage).find('#tblTasks tbody'));
					taskCountChanged();
					console.log('about to render table with server tasks loadServerTasksByTeam');
					//renderTable(); --skip for now, this just sets style class for overdue tasks 111917kl
				})

			});
		},

		/**
		 * @author cong.nguyen
		 * data is users, json format
		 */
		loadServerTasksByUser: function(users) {

			$(taskPage).find('#tblTasks tbody').empty();
			let u = users[0]; // TODO the first team
			for (let i = 1; i < users.length; i++) {
				if (users[i].id == $("#userCombo").val() ) {
					u = users[i];
				}
			}

			let tasks = u.taskList;
			$.each(tasks, function( indexTask, task) {
				console.log("ttt: " + task.id);
				console.log("ttt: " + task.name);
				if (!task.complete) {
					task.complete = false;
				}

				$('#taskRow').tmpl(task).appendTo($(taskPage).find('#tblTasks tbody'));
				taskCountChanged();
				console.log('about to render table with server tasks loadServerTasksByUser');
			})
		},

		loadServerTasksAllUser: function(users) {

			$(taskPage).find('#tblTasks tbody').empty();
			let usersSort = users;
			usersSort.sort(function (a, b) {
				// sort by name
				// return a.name.localeCompare(b.name);
				return a.id < b.id;
			});
			// teamSort.sort();
			$.each(usersSort, function (index, user) {

				let tasks = user.taskList;
				$.each(tasks, function( indexTask, task) {
					console.log("user id: " + user.id);
					console.log("task.name: " + task.name);
					if (!task.complete) {
						task.complete = false;
					}

					$('#taskRow').tmpl(task).appendTo($(taskPage).find('#tblTasks tbody'));
					taskCountChanged();
					console.log('about to render table with server tasks loadServerTasksByUser');
					//renderTable(); --skip for now, this just sets style class for overdue tasks 111917kl
				})

			});
		},
		loadTasks : function() {
			$(taskPage).find('#tblTasks tbody').empty();
			storageEngine.findAll('task', function(tasks) {
				tasks.sort(function(o1, o2) {
					return Date.parse(o1.requiredBy).compareTo(Date.parse(o2.requiredBy));
				});
				$.each(tasks, function(index, task) {
					if (!task.complete) {
						task.complete = false;
					}
					$('#taskRow').tmpl(task).appendTo($(taskPage).find('#tblTasks tbody'));
					taskCountChanged();
					renderTable();
				});
			}, errorLogger);
		},

		retrieveTasksServerLoadPage : retrieveTasksServer

	}
}();
