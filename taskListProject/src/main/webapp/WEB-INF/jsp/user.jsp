<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Users page</title>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round|Open+Sans">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <%--<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>--%>
    <%--<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>--%>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="resources/scripts/users-controller.js"></script>
    <link rel="stylesheet" type="text/css" href="resources/styles/user.css">
    <link rel="stylesheet" type="text/css" href="resources/styles/nav.css">
</head>
<body>
<div class="container">

    <div class="row">
        <div class="col-sm">
            <nav class="navbar navbar-icon-top fixed-top navbar-expand-lg navbar-light" style="background-color: #4fb9bc;">
                <a class="navbar-brand" href="#">
                    <img src="resources/images/logo.png" width="70" height="70" alt="">
                    TaskList
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse"
                        data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                        aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="home">
                                <i class="fa fa-tasks"></i>
                                Tasks
                            </a>
                        </li>
                        <li id="users-menu" class="nav-item active">
                            <a class="nav-link" href="#">
                                <i class="fa fa-users">
                                </i>
                                Users
                                <span class="sr-only">(current)</span>
                            </a>
                        </li>
                    </ul>
                    <ul class="navbar-nav ">
                        <li id="account" class="nav-item">
                            <a class="nav-link" href="#">
                                <i class="fa fa-user">
                                </i>
                                Account
                            </a>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
    </div>
    <div class="row" id="user-profile" style="display: none">
        <div class="col-sm" >
            <div class="table-wrapper">
                <div class="table-title">
                    <div class="row">
                        <div class="col-sm-4"><h4>Your Account Info: </h4></div>
                    </div>
                </div>
                <table class="table table-no-border">
                    <tbody>
                    <tr>
                        <td> Name</td>
                        <td>  ${currentUser.name}</td>
                    </tr>
                    <tr>
                        <td> TeamId</td>
                        <td>  ${currentUser.teamId}</td>
                    </tr>
                    <tr>
                        <td> Email</td>
                        <td>  ${currentUser.email}</td>
                    </tr>
                    <tr>
                        <td> Location</td>
                        <td>  ${currentUser.location}</td>
                    </tr>
                    </tbody>
                </table>
            </div>

        </div>
    </div>

    <div class="row" id="user-list">
        <div class="col-sm">
            <div class="table-wrapper">
                <div class="table-title">
                    <div class="row">
                        <div class="col-sm-4"><h2>User List</h2></div>
                        <input class="col-sm-3 form-control" type="text" id="nameInput" placeholder="name.."
                               title="name">
                        <input class="col-sm-3 form-control" type="text" id="locationInput" placeholder="location.."
                               title="location">
                        <div class="col-sm-2">
                            <button type="button" class="btn btn-info add-new"><i class="fa fa-plus"></i> Add New
                            </button>
                        </div>
                    </div>
                </div>
                <table class="table table-bordered" id="userTbl">
                    <thead>
                    <tr>
                        <th>Id</th>
                        <th>Email</th>
                        <th>Name</th>
                        <th>TeamId</th>
                        <th>Phone</th>
                        <th>Location</th>
                        <th class="text-center">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach items="${users}" var="user">
                        <tr>
                            <td>
                                <c:out value="${user.id}"/>
                            </td>
                            <td>
                                <c:out value="${user.email}"/>
                            </td>
                            <td>
                                <c:out value="${user.name}"/>
                            </td>
                            <td>
                                <c:out value="${user.teamId}"/>
                            </td>
                            <td>
                                <c:out value="${user.phone}"/>
                            </td>
                            <td>
                                <c:out value="${user.location}"/>
                            </td>
                            <td>
                                <a class="add" title="Add" data-toggle="tooltip"><i class="material-icons">&#xE03B;</i></a>
                                <a class="edit" title="Edit" data-toggle="tooltip"><i
                                        class="material-icons">&#xE254;</i></a>
                                <a class="delete" title="Delete" data-toggle="tooltip"><i class="material-icons">&#xE872;</i></a>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </div>


</div>

</body>
</html>
