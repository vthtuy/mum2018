package services;

import utility.Constants;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DataAccessImpl {

    public static Connection getConnection(){
        try {
            Class.forName("org.sqlite.JDBC");
        } catch (ClassNotFoundException c) {
            System.out.println(c.getMessage());
        }
        //keep connection
        try {
            Connection conn = DriverManager.getConnection(Constants.DATABASE_URL);
            return conn;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }
}
