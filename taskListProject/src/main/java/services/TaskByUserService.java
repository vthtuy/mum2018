package services;

import model.Task;
import model.Team;
import model.User;

import java.util.List;

public interface TaskByUserService {
    List<User> getAllUser();
}
