package services;

import model.Task;
import model.TaskLog;
import model.Team;
import model.User;

import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class TeamServiceImpl implements TeamService {

    private static final String FIND_ALL_TEAM = "SELECT * FROM team ";
    private static final String FIND_TASK_BY_TEAM = "SELECT * FROM task WHERE teamId = ? ";
    private static final String FIND_USER_BY_ID = "SELECT * FROM user WHERE id = ? ";
    private static final String INSERT_TASKLOG = "INSERT INTO tasklog(taskId, userId, note, createDate) VALUES(?, ?, ?, ?) ";

    public List<Team> getAllTeam() {
        List<Team> result = Collections.emptyList();

        Connection conn = DataAccessImpl.getConnection();
        try (PreparedStatement pstmt = conn.prepareStatement(FIND_ALL_TEAM);) {
            ResultSet resultSet = null;
            resultSet = pstmt.executeQuery();
            if (resultSet != null) {
                result = new ArrayList<>();
            }
            while (resultSet.next()) {

                Team model = new Team();
                model.setId(resultSet.getInt("id"));
                model.setName(resultSet.getString("name"));

                // get task list
                List<Task> taskList = getTaskAndTeam(model.getId());
                model.setTaskList(taskList);
                result.add(model);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    public List<Task> getTaskAndTeam(int teamId) {
        List<Task> result = Collections.emptyList();
        ResultSet resultSet = null;
        Connection conn  = DataAccessImpl.getConnection();
        try (PreparedStatement pstmt = conn.prepareStatement(FIND_TASK_BY_TEAM);) {
            // set the value
            pstmt.setInt(1, teamId);

            resultSet = pstmt.executeQuery();
            if (resultSet != null) {
                result = new ArrayList<>();
            }

            while (resultSet.next()) {
                Task model = new Task();
                model.setId(resultSet.getInt("id"));
                model.setName(resultSet.getString("name"));
                model.setTeamId(resultSet.getInt("teamId"));
                model.setCategoryId(resultSet.getInt("categoryId"));
                model.setUserId(resultSet.getInt("assignee"));
                model.setPriority(resultSet.getInt("priority"));
                model.setStatus(resultSet.getString("status"));
                model.setCategoryName(resultSet.getString("categoryId"));
                model.setCategoryId(resultSet.getInt("categoryId"));
                if (resultSet.getDate("dueDate") != null) {
                    model.setDueDate(new java.util.Date(resultSet.getDate("dueDate").getTime()));

                    // yyyy-MM-dd
                    String pattern = "yyyy-MM-dd";
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
                    String dateString = simpleDateFormat.format(model.getDueDate());
                    model.setDueDateString(dateString);
                }
                // assignee of task
                User user = getUserById(model.getUserId());
                model.setAssignee(user);

                result.add(model);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }


    public User getUserById (int id) {

        Connection conn = DataAccessImpl.getConnection();
        try (PreparedStatement pstmt = conn.prepareStatement(FIND_USER_BY_ID);) {
            ResultSet resultSet = null;
            // set the value
            pstmt.setInt(1, id);
            resultSet = pstmt.executeQuery();

            while (resultSet.next()) {

                User user = new User();
                user.setId(resultSet.getInt("id"));
                user.setEmail(resultSet.getString("email"));

                return user;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
    public boolean saveTaskLog(TaskLog taskLog) {
        boolean result = false;
        Connection conn  = DataAccessImpl.getConnection();
        // taskId, userId, note, createDate
        try (PreparedStatement pstmt = conn.prepareStatement(INSERT_TASKLOG)) {
            pstmt.setInt(1, taskLog.getTaskId());
            pstmt.setInt(2,taskLog.getUserId());
            pstmt.setString(3,taskLog.getNote());
            pstmt.setDate(4, new Date(System.currentTimeMillis()));
            result = pstmt.execute();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }
}
