package services;

import model.TaskLog;
import model.Team;
import model.User;

import java.util.List;

public interface TeamService {

    List<Team> getAllTeam();

    boolean saveTaskLog(TaskLog taskLog);

    User getUserById (int id);
}
