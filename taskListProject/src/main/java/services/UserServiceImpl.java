package services;

import model.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class UserServiceImpl implements UserService {
  private static final String FIND_ONE = "SELECT * from User u where u.email = ?";
  private static final String FIND_ALL = "SELECT * from User u";

  @Override
  public List<User> findAll() {
    List<User> userList = new ArrayList<>();
    ResultSet resultSet = null;
    Connection conn = DataAccessImpl.getConnection();
    try (PreparedStatement pstmt = conn.prepareStatement(FIND_ALL)) {
      resultSet = pstmt.executeQuery();

      while (resultSet.next()) {
        User user = new User();
        populateUserInfo(user, resultSet);
        userList.add(user);
      }

    } catch (SQLException e) {
      e.printStackTrace();
    }
    return userList;
  }

  @Override
  public void update(User user) {}

  public User findByUsername(String username) {
    User user = null ;
    ResultSet resultSet = null;
    Connection conn = DataAccessImpl.getConnection();
    try (PreparedStatement pstmt = conn.prepareStatement(FIND_ONE)) {
      pstmt.setString(1, username);
      resultSet = pstmt.executeQuery();

      while (resultSet.next()) {
          user = new User();
        populateUserInfo(user, resultSet);

      }
        return user;
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return null;
  }

  private void populateUserInfo(User user, ResultSet resultSet) throws SQLException {
    user.setId(resultSet.getInt("id"));
    user.setEmail(resultSet.getString("email"));
    user.setName(resultSet.getString("name"));
    user.setTeamId(resultSet.getInt("teamId"));
    user.setPhone(resultSet.getString("phone"));
    user.setLocation(resultSet.getString("location"));
  }
}
