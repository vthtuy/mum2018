package services;

import model.User;

import java.util.List;


public interface UserService {
    User findByUsername(String username);
    List<User> findAll();
    void update(User uSer);
}
