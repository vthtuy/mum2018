package services;

import model.Task;

import java.util.List;

public interface TaskService {
    public List<Task> findTasks();
    public boolean insertTask(Task task);
    public boolean updateTask(Task task);
    public boolean changeStatusTask(Task task);
    public boolean removeTask(Task task);
}
