package services;

import model.Task;
import model.Team;
import model.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class TaskByUserImpl implements TaskByUserService {

    private static final String FIND_ALL_USER = "SELECT * FROM user ";
    private static final String FIND_TASK_BY_USER = "SELECT * FROM task WHERE assignee = ? ";
    private static final String FIND_USER_BY_ID = "SELECT * FROM user WHERE id = ? ";
    private static final String INSERT_TASKLOG = "INSERT INTO tasklog(taskId, userId, note, createDate) VALUES(?, ?, ?, ?) ";

    public List<User> getAllUser() {
        List<User> result = Collections.emptyList();

        Connection conn = DataAccessImpl.getConnection();
        try (PreparedStatement pstmt = conn.prepareStatement(FIND_ALL_USER);) {
            ResultSet resultSet = null;
            resultSet = pstmt.executeQuery();
            if (resultSet != null) {
                result = new ArrayList<>();
            }
            while (resultSet.next()) {

                User model = new User();
                model.setId(resultSet.getInt("id"));
                model.setName(resultSet.getString("name"));

                // get task list
                List<Task> taskList = getTaskByUser(model.getId());
                model.setTaskList(taskList);
                result.add(model);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    public List<Task> getTaskByUser(int userId) {
        List<Task> result = Collections.emptyList();
        ResultSet resultSet = null;
        Connection conn  = DataAccessImpl.getConnection();
        try (PreparedStatement pstmt = conn.prepareStatement(FIND_TASK_BY_USER);) {
            // set the value

            pstmt.setInt(1, userId);

            resultSet = pstmt.executeQuery();
            if (resultSet != null) {
                result = new ArrayList<>();
            }

            while (resultSet.next()) {
                Task model = new Task();
                model.setId(resultSet.getInt("id"));
                model.setName(resultSet.getString("name"));
                model.setTeamId(resultSet.getInt("teamId"));
                model.setCategoryId(resultSet.getInt("categoryId"));
                model.setUserId(resultSet.getInt("assignee"));
                model.setPriority(resultSet.getInt("priority"));
                model.setStatus(resultSet.getString("status"));
                model.setCategoryName(resultSet.getString("categoryId"));
                model.setCategoryId(resultSet.getInt("categoryId"));

                // assignee of task
                User user = getUserById(model.getUserId());
                model.setAssignee(user);

                result.add(model);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }


    public User getUserById (int id) {

        Connection conn = DataAccessImpl.getConnection();
        try (PreparedStatement pstmt = conn.prepareStatement(FIND_USER_BY_ID);) {
            ResultSet resultSet = null;
            // set the value
            pstmt.setInt(1, id);
            resultSet = pstmt.executeQuery();

            while (resultSet.next()) {

                User user = new User();
                user.setId(resultSet.getInt("id"));
                user.setEmail(resultSet.getString("email"));

                return user;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
