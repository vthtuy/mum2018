package services;

import model.Task;
import model.User;

import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class TaskServiceImpl implements TaskService {

    private static final String FIND_TASKS = "SELECT t.id, t.name , dueDate, c.id categoryId, c.name catName, priority, u.id uid, u.email, t.status " +
            "FROM task as t LEFT JOIN category as c ON t.categoryId = c.id LEFT JOIN user as u ON t.assignee = u.id";
    private static final String INSERT_TASK = "INSERT INTO task(categoryId, name, dueDate, priority, assignee) VALUES(?,?,?,?,?)";
    private static final String COMPLETE_TASK = "UPDATE task SET status = ? WHERE id =?";
    private static final String UPDATE_TASK = "UPDATE task SET name=?,dueDate=?,categoryId=?,priority=?,assignee=?,status=? WHERE id =?";
    private static final String DELETE_TASK = "DELETE FROM task WHERE id =?";
    public List<Task> findTasks() {
        List<Task> result = Collections.emptyList();
        ResultSet resultSet = null;
        Connection conn  = DataAccessImpl.getConnection();
        try (PreparedStatement pstmt = conn.prepareStatement(FIND_TASKS)) {

            resultSet = pstmt.executeQuery();
            if (resultSet != null) {
                result = new ArrayList<>();
            }
            while (resultSet.next()) {
                Task model = new Task();
                model.setId(resultSet.getInt("id"));
                model.setName(resultSet.getString("name"));
                if (resultSet.getDate("dueDate") != null) {
                    model.setDueDate(new java.util.Date(resultSet.getDate("dueDate").getTime()));

                    // yyyy-MM-dd
                    String pattern = "yyyy-MM-dd";
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
                    String dateString = simpleDateFormat.format(model.getDueDate());
                    model.setDueDateString(dateString);
                }
                model.setCategoryName(resultSet.getString("catName"));
                model.setCategoryId(resultSet.getInt("categoryId"));

                model.setPriority(resultSet.getInt("priority"));
                model.setStatus(resultSet.getString("status"));
                User user = new User();
                user.setId(resultSet.getInt("uid"));
                user.setEmail(resultSet.getString("email"));
                model.setAssignee(user);
                result.add(model);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    public boolean insertTask(Task task) {
        boolean result = false;
        Connection conn  = DataAccessImpl.getConnection();
        try (PreparedStatement pstmt = conn.prepareStatement(INSERT_TASK)) {
            pstmt.setInt(1, task.getCategoryId());
            pstmt.setString(2,task.getName());
            pstmt.setDate(3, new Date(task.getDueDate().getTime()));
            pstmt.setInt(4, task.getPriority());
            pstmt.setInt(5, task.getAssignee().getId());

            result = pstmt.execute();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    public boolean updateTask(Task task) {
        boolean result = false;
        Connection conn  = DataAccessImpl.getConnection();
        try (PreparedStatement pstmt = conn.prepareStatement(UPDATE_TASK)) {
            pstmt.setString(1, task.getName());
            pstmt.setDate(2, new Date(task.getDueDate().getTime()));
            pstmt.setInt(3, task.getCategoryId());
            pstmt.setInt(4, task.getPriority());
            pstmt.setInt(5, task.getAssignee().getId());
            pstmt.setString(6, task.getStatus());
            pstmt.setInt(7, task.getId());
            result = pstmt.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    public boolean changeStatusTask(Task task) {
        boolean result = false;
        Connection conn  = DataAccessImpl.getConnection();
        try (PreparedStatement pstmt = conn.prepareStatement(COMPLETE_TASK)) {
            pstmt.setString(1, task.getStatus());
            pstmt.setInt(2, task.getId());
            result = pstmt.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    public boolean removeTask(Task task) {
        boolean result = false;
        Connection conn  = DataAccessImpl.getConnection();
        try (PreparedStatement pstmt = conn.prepareStatement(DELETE_TASK)) {
            pstmt.setInt(1, task.getId());
            result = pstmt.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }
}
