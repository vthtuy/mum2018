package utility;

/**
 *
 */
public class Constants {

    public static final String URL_PRE_PATH = System.getProperty("catalina.base");

    public static final String DATA_DIRECTORY =  URL_PRE_PATH + "/data";

   // public static final String DATABASE_URL = "jdbc:sqlite:" + URL_PRE_PATH + "/data/database.db";

    public static final String DATABASE_URL = "jdbc:sqlite:/data/database.db";
    public static final String SQL_PATH = "/data/sql.sql";

    public static final String DATA_INSERT_PATH = "/data/data.sql";

    public static final String JSP_INIT = "/jsp/init.jsp";

    public static final String DATE_FORMAT = "yyyy-MM-dd";

    public static final String JSP_TASK_OF_TEAM = "/jsp/taskOfTeam.jsp";

    public static final String JSON_TASK_OF_TEAM = "/jsp/fragment/taskOfTeamJson.jsp";

}
