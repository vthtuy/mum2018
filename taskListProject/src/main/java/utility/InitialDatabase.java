package utility;


import org.apache.commons.io.FileUtils;
import services.DataAccessImpl;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Logger;

public class InitialDatabase {

    private static final Logger LOGGER = Logger.getLogger(InitialDatabase.class.getName());

    private static String SQL_PATH = Constants.URL_PRE_PATH + Constants.SQL_PATH;

    private static String DATA_INSERT_PATH = Constants.URL_PRE_PATH + Constants.DATA_INSERT_PATH;

    /*
     * create library database
     */
    public static void createNewDatabase() {
        System.out.println("Database: " + Constants.DATABASE_URL);
        try (Connection conn = DataAccessImpl.getConnection()) {
            //try (Connection conn = DriverManager.getConnection(url)) {
            if (conn != null) {
                DatabaseMetaData meta = conn.getMetaData();
                System.out.println("The driver name is " + meta.getDriverName());
                System.out.println("A new database has been created.");
            }

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * Create a new table from file
     *
     * @throws IOException
     */
    public static void createNewTable() throws IOException {
        System.out.println(" File DB structure: " + SQL_PATH);
        // SQL statement for creating a new table
        StringBuilder sql = new StringBuilder();

        byte[] encoded = Files.readAllBytes(Paths.get(SQL_PATH));
        String createTableSql = new String(encoded, StandardCharsets.UTF_8);
        // StringBuilder sql = new StringBuilder(encoded);

        String[] array = createTableSql.split(";");

        sql.append(createTableSql);

        try (Connection conn = DataAccessImpl.getConnection(); Statement stmt = conn.createStatement()) {
            // create a new tables
            for (int i = 0; i < array.length; i++) {
                System.out.println(array[i]);

                stmt.execute(array[i].toString());
            }
            conn.commit();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void insertDataFromFile() throws IOException {
        System.out.println("File Database data: " + DATA_INSERT_PATH);
        // SQL statement for creating a new table
        StringBuilder sql = new StringBuilder();

        byte[] encoded = Files.readAllBytes(Paths.get(DATA_INSERT_PATH));
        String dataSql = new String(encoded, StandardCharsets.UTF_8);
        // StringBuilder sql = new StringBuilder(encoded);

        LOGGER.info("====== INSERT DATA =============");

        sql.append(dataSql);
        System.out.println(dataSql);

        try (Connection conn = DataAccessImpl.getConnection(); Statement stmt = conn.createStatement()) {
            // create a new tables
            stmt.executeUpdate(sql.toString());
            // conn.commit();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void copyDataToTomcat(String url_pre_path) {
        System.out.println("copy from ....." + url_pre_path + Constants.SQL_PATH);
        System.out.println("to ....." + SQL_PATH);
        //C:\soft\apache-tomcat-9.0.13\webapps\tasklist-feb

        File sourceTable = new File(url_pre_path + Constants.SQL_PATH);
        File destTable = new File(SQL_PATH);

        File sourceInsertData = new File(url_pre_path + Constants.DATA_INSERT_PATH);
        File destTableInsertData = new File(DATA_INSERT_PATH);
        try {
            FileUtils.copyFile(sourceTable, destTable);
            FileUtils.copyFile(sourceInsertData, destTableInsertData);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        createNewDatabase();

        try {
            createNewTable();
            insertDataFromFile();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        //
        InitialDatabase app = new InitialDatabase();
        // insert three new rows
        // app.insert("Raw Materials", 3000);
        // app.insert("Semifinished Goods", 4000);
        // app.insert("Finished Goods", 5000);
    }
}
