package controller;

import com.google.gson.Gson;
import model.Task;
import model.User;
import services.TaskService;
import services.TaskServiceImpl;
import utility.Constants;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

@WebServlet("/TaskServlet")
public class TaskServlet extends HttpServlet {

    TaskService taskService = new TaskServiceImpl();
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Task task = prepareTask(request);
        String action = request.getParameter("action");
        if(action != null){
            if(action.equals("delete")){
                taskService.removeTask(task);
            } else if(action.equals("update")){
                taskService.updateTask(task);
            } else if(action.equals("changeStatus")){
                taskService.changeStatusTask(task);
            } else if(action.equals("add")){
                taskService.insertTask(task);
            }
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter out = response.getWriter();

        String JSONtasks;
        List<Task> taskList = taskService.findTasks();
        JSONtasks = new Gson().toJson(taskList);

        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        out.write(JSONtasks);
    }

    private Task prepareTask(HttpServletRequest request){
        Task task = new Task();
        if(request.getParameter("id") != null && request.getParameter("id").trim() != "" ){
            task.setId(Integer.parseInt(request.getParameter("id")));
        }

        if(request.getParameter("name") != null){
            task.setName(request.getParameter("name"));
        }

        if(request.getParameter("priority") != null){
            task.setPriority(Integer.parseInt(request.getParameter("priority")));
        }

        User user = new User();
        //TODO get real user id when user management complete
        user.setId(1);
        task.setAssignee(user);

        if(request.getParameter("dueDate") != null){
            try {
                SimpleDateFormat fm = new SimpleDateFormat(Constants.DATE_FORMAT);
                task.setDueDate(fm.parse(request.getParameter("dueDate")));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        if(request.getParameter("categoryId") != null){
            task.setCategoryId(Integer.parseInt(request.getParameter("categoryId")));
        }

        if(request.getParameter("status") != null){
            task.setStatus(request.getParameter("status"));
        }

        return task;
    }
}
