package controller;

import model.Team;
import model.User;
import services.TeamService;
import services.TeamServiceImpl;
import services.UserService;
import services.UserServiceImpl;
import utility.Constants;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/home")
public class TeamServlet  extends HttpServlet {

    private TeamService teamService = new TeamServiceImpl();
    private UserService userService = new UserServiceImpl();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<Team> listTeam = teamService.getAllTeam();
        req.setAttribute("listTeam", listTeam);

        List<User> listUser = userService.findAll();
        req.setAttribute("listUser", listUser);

        RequestDispatcher view = req.getRequestDispatcher(Constants.JSP_TASK_OF_TEAM);
        view.forward(req, resp);
    }
}
