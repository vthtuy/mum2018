package controller;

import utility.Constants;
import utility.InitialDatabase;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;

@WebServlet("/init")
public class InitialDataServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher view = req.getRequestDispatcher(Constants.JSP_INIT);
        view.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        System.out.println("App Deployed Directory path: " + getServletContext().getRealPath(File.separator));
        String result = "";
        String s = getServletContext().getRealPath(File.separator) ;
        try {
            InitialDatabase.copyDataToTomcat(s);

            InitialDatabase.createNewDatabase();
        } catch (Exception e) {
            result = "error";
        }

        try {
            InitialDatabase.createNewTable();
            InitialDatabase.insertDataFromFile();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        if (!result.isEmpty()) {
            result = "success";
        }
        req.setAttribute("result", result);
        RequestDispatcher view = req.getRequestDispatcher(Constants.JSP_INIT);
        view.forward(req, resp);
    }
}
