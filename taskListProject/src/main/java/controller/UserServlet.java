package controller;

import model.User;
import services.UserService;
import services.UserServiceImpl;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "UserServlet", urlPatterns = "/user")
public class UserServlet extends HttpServlet {
  private UserService userService = new UserServiceImpl();

  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    String username = request.getParameter("username");
    String password = request.getParameter("password");
    if(isValidLogin(username, password)) {
      RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/login.jsp");
      dispatcher.forward(request, response);
    }

    User currentUser = userService.findByUsername(username);
    request.getSession().setAttribute("currentUser", currentUser);

    response.sendRedirect(request.getContextPath() + "/home");

  }

  protected void doGet(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {

    List<User> users = userService.findAll();
    request.setAttribute("users", users);
    RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/WEB-INF/jsp/user.jsp");
    dispatcher.forward(request, response);


  }

  private boolean isValidLogin(String username, String password) {
    return username == null || password == null || username.isEmpty() || password.isEmpty();
  }


}
