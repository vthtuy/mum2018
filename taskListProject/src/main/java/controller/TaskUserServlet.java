package controller;

import com.google.gson.Gson;
import model.User;
import services.TaskByUserImpl;
import services.TaskByUserService;
import utility.Constants;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "TaskUserServlet", urlPatterns = "/taskuser")
public class TaskUserServlet extends HttpServlet {
    TaskByUserService service = new TaskByUserImpl();

    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        List<User> listUser = service.getAllUser();

        String JSONtasks = new Gson().toJson(listUser);
        req.setAttribute("JSONtasks", JSONtasks);

        RequestDispatcher view = req.getRequestDispatcher(Constants.JSON_TASK_OF_TEAM);
        view.forward(req, resp);
    }
}
