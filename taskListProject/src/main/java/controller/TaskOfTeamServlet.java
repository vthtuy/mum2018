package controller;

import com.google.gson.Gson;
import model.Task;
import model.TaskLog;
import model.Team;
import model.User;
import services.TeamService;
import services.TeamServiceImpl;
import utility.Constants;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@WebServlet("/TaskOfTeamServlet")
public class TaskOfTeamServlet extends HttpServlet {

    private TeamService teamService = new TeamServiceImpl();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        List<Team> listTeam = teamService.getAllTeam();
        String JSONtasks = new Gson().toJson(listTeam);
        req.setAttribute("JSONtasks", JSONtasks);

        RequestDispatcher view = req.getRequestDispatcher(Constants.JSON_TASK_OF_TEAM);
        view.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String taskId = req.getParameter("taskId");
        String note = req.getParameter("note");
        TaskLog model = new TaskLog();
        model.setTaskId(Integer.parseInt(taskId));

        System.out.println("currentUser: " + req.getSession().getAttribute("currentUser"));

        User loginUser = req.getSession().getAttribute("currentUser") != null ? (User) req.getSession().getAttribute("currentUser") : null;

        Map<String, String > result = new HashMap<>();
        if (loginUser != null) {
            model.setUserId(loginUser.getId());
            model.setNote(note);
            teamService.saveTaskLog(model);
            result.put("result", "success");
        } else {
            result.put("result", "notLogin");
        }
        req.setAttribute("JSONtasks", new Gson().toJson(result));

        RequestDispatcher view = req.getRequestDispatcher(Constants.JSON_TASK_OF_TEAM);
        view.forward(req, resp);
    }
}
