 -- role admin=1; role employee=2; both=3
INSERT INTO employee(id,  firstname,  lastname, phone , account , password , role  ) VALUES
("admin", "Admin", "Admin", "0433343", "", "123", 1),
("admin2", "Admin2", "Admin", "0433343", "", "123", 1),
("admin3", "Admin 3", "Admin", "0433343", "", "123", 1),
("admin4", "Admin 4", "Admin", "0433343", "", "123", 1),

-- librarian
("lib", "Librarian", "Librarian", "0433343", "", "123", 0),
("lib2", "Librarian 2", "Librarian", "0433343", "", "123", 0),
("lib3", "Librarian 3", "Librarian", "0433343", "", "123", 0),
("lib4", "Librarian 4", "Librarian", "0433343", "", "123", 0),

-- both
("both", "Both", "Both", "0433343", "", "123", 3),
("both2", "Both 2", "Both", "0433343", "", "123", 3),
("both3", "Both 3", "Both", "0433343", "", "123", 3),
("both4", "Both 4", "Both", "0433343", "", "123", 3)
;

INSERT INTO member(id, firstname,lastname) VALUES
("one","One Two", "one"),
("two","One Two", "two"),
("three","One Two", "Three")
;

INSERT INTO Bookinfor(id, title, isbn, maxBorrowDays ) VALUES 
("001","There are some interfaces ", "one", 21 ),
("002","Title Two", "two", 21 ),
("003","Title Three", "Three", 21 ),
("004","Don Quixote ", "four", 7 ),
("005"," In Search of Lost Time", "five", 21 )
;
INSERT INTO author( id, firstname, lastname, phone, bio) VALUES
("1","Author", "one", "123456", "simple"),
("2","John", "Kenedy", "+75463543", "life is progressing"),
("3","Stack", "Overflow", "+89675646", "rest & action"),
("4","Barbara", "Brooks", "+8145454", "wholeness"),
("5","Steven", "McPhee", "+454677", "intelligent")
;
INSERT INTO BookAuthor(  author_id, book_id) VALUES
("1","001"),
("2","001"),
("2","002"),
("3","003"),
("3","005"),
("2","005"),
("5","005")
;
-- availability=1 means available to check out; 0=false means someone has already checked out.
INSERT INTO BookCopy(book_id, availability) VALUES
("001", 1),
("001", 1),
("002", 1),
("002", 0),
("003", 1)
;

 