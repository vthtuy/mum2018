CREATE TABLE IF NOT EXISTS employee(
  id varchar PRIMARY KEY,
  firstname varchar,
  lastname varchar,
 phone varchar, account varchar, password varchar, role varchar NOT NULL
);
CREATE TABLE IF NOT EXISTS member(
  id varchar PRIMARY KEY,
  firstname varchar,
  lastname varchar,
 phone varchar
);

CREATE TABLE IF NOT EXISTS author(
  id varchar PRIMARY KEY,
  firstname varchar,
  lastname varchar,
  phone varchar,
  bio varchar
);

CREATE TABLE IF NOT EXISTS address(
  id INTEGER PRIMARY KEY,
  emp_id varchar,
  author_id varchar,
  mem_id varchar,
  street varchar,
  city varchar,
  state varchar,
  zip varchar
);

CREATE TABLE IF NOT EXISTS Bookinfor(
  id varchar PRIMARY KEY,
  		title varchar,
		isbn varchar,
		maxBorrowDays int
);

CREATE TABLE IF NOT EXISTS BookCopy(  
	id INTEGER PRIMARY KEY,
  book_id varchar,
   availability boolean
);

CREATE TABLE IF NOT EXISTS CheckoutEntity(  
  mem_id varchar,
  bookcopy_id INTEGER,
  chekoutdate date,
  dueDate date
);

CREATE TABLE IF NOT EXISTS BookAuthor(  

		id INTEGER PRIMARY KEY,
  author_id varchar,
  book_id varchar
);

