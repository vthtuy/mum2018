package application;
	
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.MenuBar;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import mpp.Constances;
import mpp.controller.MenuAreaGenerator;


public class Main extends Application {
	
	
	
	@Override
	public void start(Stage primaryStage) {
		try {
			VBox a = new VBox();
			VBox topContainer = new VBox();
			Parent ui = FXMLLoader.load(getClass().getResource(Constances.VIEW_MEMBER_UI));
			
			MenuAreaGenerator menuGenerator = new MenuAreaGenerator();
			MenuBar mainMenu = menuGenerator.generateMenuByRole(null);

			topContainer.getChildren().add(mainMenu);
			topContainer.getChildren().add(ui);

			Scene scene = new Scene(topContainer, Constances.N_800, Constances.N_600);
			
			scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			primaryStage.setScene(scene);
			primaryStage.show();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		launch(args);
	}
}
