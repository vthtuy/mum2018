package application;

import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import mpp.Constances;

public class MemberWindow extends Stage {
	private TableView table = new TableView<>();
	Text messageBar = new Text();
	Stage mainWindow;

	public void setData(ObservableList items) {
		ObservableList current = table.getItems();
		if (current != null) {
			current.addAll(items);
		}
		table.setItems(current);
	}

	public MemberWindow(Stage mainWindow) {

		this.mainWindow = mainWindow;
		setTitle("Library Member Info");
		messageBar.setFill(Color.FIREBRICK);

		final Label label = new Label(String.format("Library Member Info"));
		label.setFont(new Font("Arial", 16));
		HBox labelHbox = new HBox(10);
		labelHbox.setAlignment(Pos.CENTER);
		labelHbox.getChildren().add(label);

		table.setEditable(false);
		TableColumn indexCol = new TableColumn<>("Index");
		indexCol.setMinWidth(80);

		// table.getColumns().addAll(indexCol, checkoutDateCol, dueDateCol, pubTypeCol,
		// titleCol, isbnIssueNumCol, copyNumCol);

		GridPane grid = new GridPane();
		grid.setAlignment(Pos.CENTER);
		grid.setVgap(10);
		grid.setHgap(10);
		grid.add(labelHbox, 0, 0);
		grid.add(table, 0, 1);
		grid.add(messageBar, 0, 2);

		Scene scene = new Scene(grid, Constances.N_800, Constances.N_600);
		setScene(scene);
	}
}