package application;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.image.Image;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
//import project.business.ControllerInterface;
//import project.business.LibraryMember;
//import project.business.SystemController;
import mpp.Constances;
import mpp.Constances.Role;
import mpp.controller.MenuAreaGenerator;
import mpp.entity.MemberModel;

public class LoginWindow extends Application {

	private MemberWindow memberWindow;

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) {
		primaryStage.setTitle("Vitamin Library");

		try {
			VBox topContainer = new VBox();
			
			Parent loginUI = FXMLLoader.load(getClass().getResource(Constances.VIEW_LOGIN_UI));
			
			// loginUI.setId("loginLeftArea");
			
			MenuAreaGenerator menuGenerator = new MenuAreaGenerator();
			MenuBar mainMenu = menuGenerator.generateMenuByRole(null);

			topContainer.getChildren().add(mainMenu);
			topContainer.getChildren().add(loginUI);

			Scene scene = new Scene(topContainer, Constances.N_800, Constances.N_600);
			// scene.getStylesheets().add(getClass().getResource("library.css").toExternalForm());
			scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			
			primaryStage.getIcons().add( new Image( getClass().getResourceAsStream( "icon.png" ))); 
			primaryStage.setScene(scene);

			primaryStage.show();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
