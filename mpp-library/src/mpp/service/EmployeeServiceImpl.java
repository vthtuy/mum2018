package mpp.service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import mpp.Constances;
import mpp.Constances.Role;
import mpp.entity.EmployeeModel;

public class EmployeeServiceImpl {

	private static final Logger LOGGER = Logger.getLogger(EmployeeServiceImpl.class.getName());

	public static final String url = Constances.DATABASE_URL;
	
	private static String FIND_EMPLOYEE_BY_ID = "select * from employee where id = ? ";

	private DataAccessImpl dataAccess = new DataAccessImpl();

	public EmployeeModel getRoleById2(String user) {
			List<EmployeeModel> results = new ArrayList<>();
			try (Connection conn = DriverManager.getConnection(url);
					PreparedStatement pstmt = conn.prepareStatement(FIND_EMPLOYEE_BY_ID);) {

				// set the value
				pstmt.setString(1, user);
				ResultSet resultSet = pstmt.executeQuery();

				while (resultSet.next()) {

					EmployeeModel model = new EmployeeModel();
					model.setId(resultSet.getString(1));
					model.setFirstName(resultSet.getString(2));
					model.setLastName(resultSet.getString(3));

					model.setRole(resultSet.getString("role"));
					
					if (model != null) {
						Role roleEnum = Role.LIBRARIAN;
						if ("1".equals(model.getRole())) {
							roleEnum = Role.ADMIN;
						} else if ("3".equals(model.getRole())) {
							roleEnum = Role.BOTH;
						}
						model.setRoleEnum(roleEnum);
					}
					return model;
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
  
		return null;
	}
	
	/**
	 * it is returning null :(
	 * @param user
	 * @return
	 */
	@Deprecated
	public EmployeeModel getRoleById(String user) {
		Connection conn = null;
		try {
			conn = dataAccess.getConnection();
			PreparedStatement pstmt = conn.prepareStatement(FIND_EMPLOYEE_BY_ID);
			pstmt.setString(1, user);

			ResultSet resultSet = dataAccess.findFlexibleStatement(user, pstmt);

			List<EmployeeModel> results = new ArrayList<>();

			while (resultSet.next()) {
				EmployeeModel model = new EmployeeModel();
				model.setId(resultSet.getString(1));
				model.setFirstName(resultSet.getString(2));
				model.setLastName(resultSet.getString(3));

				model.setRole(resultSet.getString("role"));
				results.add(model);
			}

			EmployeeModel model = !results.isEmpty() ? results.get(0) : null;

			System.out.println(user);
			System.out.println(model);
			
			if (model != null) {
				Role roleEnum = Role.LIBRARIAN;
				if ("1".equals(model.getRole())) {
					roleEnum = Role.ADMIN;
				} else if ("3".equals(model.getRole())) {
					roleEnum = Role.BOTH;
				}
				model.setRoleEnum(roleEnum);
			}
			return model;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return null;
	}

	public EmployeeModel login(String user, String password) {
		List<EmployeeModel> results = dataAccess.findEmployeeByIdPassword(user, password);
		EmployeeModel model = !results.isEmpty() ? results.get(0) : null;
		if (model != null) {
			Role roleEnum = Role.LIBRARIAN;
			System.out.println("model.getRole(): " + model.getRole());
			if ("1".equals(model.getRole())) {
				roleEnum = Role.ADMIN;
			} else if ("3".equals(model.getRole())) {
				roleEnum = Role.BOTH;
			}
			model.setRoleEnum(roleEnum);
		}
		return model;
	}

}
