package mpp.service;

import java.lang.reflect.Member;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Logger;

import mpp.Constances;
import mpp.entity.Address;
import mpp.entity.Author;
import mpp.entity.BookCopies;
import mpp.entity.Books;
import mpp.entity.EmployeeModel;
import mpp.entity.MemberModel;

public class DataAccessImpl {

	private static final Logger LOGGER = Logger.getLogger(DataAccessImpl.class.getName());

	public static final String url = Constances.DATABASE_URL; // "jdbc:sqlite:" + "data/library.db"

	private static String FIND_EMPLOYEE_BY_ID_PASSWORD = "select * from employee where id = ? and password = ?";
	
	private static String FIND_ALL_MEMBERS = "SELECT * FROM member m LEFT JOIN address a ON m.id = a.mem_id";

	private static String FIND_BOOK_BY_ISBN = "select * from Bookinfor where isbn = ?";
	
	public Connection getConnection() throws SQLException {
		return DriverManager.getConnection(url);
	}

	public ResultSet findFlexibleStatement(String sql, PreparedStatement pstmt) {
		ResultSet resultSet = null;

		try {
			resultSet = pstmt.executeQuery();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				pstmt.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return resultSet;

	}

	public List<EmployeeModel> findEmployeeByIdPassword(String user, String password) {
		List<EmployeeModel> result = Collections.emptyList();
		ResultSet resultSet = null;

		try (Connection conn = DriverManager.getConnection(url);
				PreparedStatement pstmt = conn.prepareStatement(FIND_EMPLOYEE_BY_ID_PASSWORD);) {

			// set the value
			pstmt.setString(1, user);
			pstmt.setString(2, password);
			resultSet = pstmt.executeQuery();
			if (resultSet != null) {
				result = new ArrayList<>();
			}
			while (resultSet.next()) {

				EmployeeModel model = new EmployeeModel();
				model.setId(resultSet.getString(1));
				model.setFirstName(resultSet.getString(2));
				model.setLastName(resultSet.getString(3));

				model.setRole(resultSet.getString("role"));
				result.add(model);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}
	
	public Books findBookByIsbn(String Isbn) {

		Books book=null;
		ResultSet result = null;


		String sql = "select * from Bookinfor where isbn = ?";

        try (Connection conn = DriverManager.getConnection(url);PreparedStatement stmt=conn.prepareStatement(sql)) {
        	stmt.setString(1, Isbn);
			result = stmt.executeQuery();
			System.out.println("result--->> "+ result);
			if (result != null) {
				book = new Books();
				book.setId(result.getString(1));;
				book.setTitle(result.getString(2));
				book.setIsbn(result.getString(3));
				book.setMaxBorrowDays(Integer.parseInt(result.getString(4)));
				}
		} catch (SQLException e) {
			System.out.println("this---->>>>>> "+e.getMessage());
		}

		return book;
	}
	
	
public List<MemberModel> findAllMembers() {

		List<MemberModel> members = Collections.emptyList();
		ResultSet result = null;

		String sql = FIND_ALL_MEMBERS;

		try (Connection conn = DriverManager.getConnection(url); Statement stmt = conn.createStatement()) {

			result = stmt.executeQuery(sql);
			if (result != null) {
				members = new ArrayList<>();
			}

			MemberModel member;
			Address addr;
			while (result.next()) {
				member = new MemberModel();
				member.setId(result.getString(1));
				member.setFirstName(result.getString("firstname"));
				member.setLastName(result.getString("lastname"));
				member.setPhone(result.getString("phone"));
				
				addr = new Address();
				addr.setId(result.getInt(5));
				addr.setStreet(result.getString("street"));
				addr.setCity(result.getString("city"));
				addr.setState(result.getString("state"));
				addr.setZip(result.getString("zip"));
				member.setAddress(addr);
				
				members.add(member);
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}

		return members;
	}
	
	public boolean insertMember(MemberModel member) {
        
        // SQL statement for creating a new table
        String sql = "INSERT INTO member(id,  firstname,  lastname, phone) VALUES(?,?,?,?)";
        boolean result = true;
        try (Connection conn = DriverManager.getConnection(url);PreparedStatement stmt=conn.prepareStatement(sql)) {
        	stmt.setString(1, member.getId());
        	stmt.setString(2, member.getFirstName());
        	stmt.setString(3, member.getLastName());
        	stmt.setString(4, member.getPhone());
        	stmt.execute();   
        	insertMemberAddress(member.getAddress(), member.getId());
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            result = false;
        } 
        return result;
    }
	
	public boolean insertMemberAddress(Address address, String ownerId) {
        
        // SQL statement for creating a new table
        String sql = "INSERT INTO address(mem_id, street,  city, state , zip) VALUES(?, ?,?,?,?)";
        boolean result = false;
        try (Connection conn = DriverManager.getConnection(url);PreparedStatement stmt=conn.prepareStatement(sql)) {
        	stmt.setString(1, ownerId);
        	stmt.setString(2, address.getStreet());
        	stmt.setString(3, address.getCity());
        	stmt.setString(4, address.getState());
        	stmt.setString(5, address.getZip());
        	result = stmt.execute();        	      	
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            result = false;
        } 
        return result;
    }
	
	public void updateMember(MemberModel member) {
        
		String sql = "UPDATE member set firstname =?,  lastname =?, phone =? WHERE id = ?";
        boolean result = false;
        try (Connection conn = DriverManager.getConnection(url);PreparedStatement stmt=conn.prepareStatement(sql)) {        
        	stmt.setString(1, member.getFirstName());
        	stmt.setString(2, member.getLastName());
        	stmt.setString(3, member.getPhone());
        	stmt.setString(4, member.getId());
        	result = stmt.execute();   
        	
        	Address addr = member.getAddress();
        	if(addr.getId() == 0)
        	{
        		insertMemberAddress(addr, member.getId());
        	}
        	else
        	{
        		updateAddress(addr);
        	}
        	
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        } 
    }
	
	public void updateAddress(Address addr) {
        
		String sql = "UPDATE address set street =?,  city =?, state =?, zip=? WHERE id = ?";
        boolean result = false;
        try (Connection conn = DriverManager.getConnection(url);PreparedStatement stmt=conn.prepareStatement(sql)) {        
        	stmt.setString(1, addr.getStreet());
        	stmt.setString(2, addr.getCity());
        	stmt.setString(3, addr.getState());
        	stmt.setString(4, addr.getZip());
        	stmt.setInt(5, addr.getId());
        	result = stmt.execute();        	      	
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        } 
    }
	
	public int inserTable(String query) {
		
		int result = 0;
		try (Connection conn = DriverManager.getConnection(url); Statement stmt = conn.createStatement()) {
            String op = query;
            result =  stmt.executeUpdate(op); 
            stmt.close();
            
			
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
			
		return result; 
	}
	

	public String checkTable(String query) {
		
		ResultSet result = null;
		String res = "";
		try (Connection conn = DriverManager.getConnection(url); Statement stmt = conn.createStatement()) {
            String op = query;
            result =  stmt.executeQuery(op);
            res =  result.getString(1); 
            stmt.close();
            
			
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
			
		return res; 
	}

	/**
	 * @param args the command line arguments
	 */
	public static void main(String[] args) {
	}

	public boolean saveBook(Books book) {
		 String sql = "INSERT INTO Bookinfor(id,title,isbn,maxBorrowDays) VALUES(?,?,?,?)";
	        boolean result = false;
	        try (Connection conn = DriverManager.getConnection(url);PreparedStatement stmt=conn.prepareStatement(sql)) {
	        	stmt.setString(1, book.getId());
	        	stmt.setString(2, book.getTitle());
	        	stmt.setString(3, book.getIsbn());
	        	stmt.setString(4, String.valueOf(book.getMaxBorrowDays()));
	        	result = stmt.execute();        	      	
	        } catch (SQLException e) {
	            System.out.println(e.getMessage());
	            e.printStackTrace();
	        } 
	        return result;
	}

	public boolean saveCopies(Books book) {
		// TODO Auto-generated method stub
		 String sql = "INSERT INTO BookCopy(book_id,availability) VALUES(?,?)";
	        boolean result = false;
	        try (Connection conn = DriverManager.getConnection(url);PreparedStatement stmt=conn.prepareStatement(sql)) {
	        	stmt.setString(1, book.getId());
	        	stmt.setBoolean(2, true);
	        	result = stmt.execute();        	      	
	        } catch (SQLException e) {
	            System.out.println(e.getMessage());
	            e.printStackTrace();
	        } 
	        return result;
	}

	public boolean saveAuthor(List<String> authorIdList, String bookId) {
		// TODO Auto-generated method stub
		 boolean result = false;
		for(String a: authorIdList) {
			String sql = "INSERT INTO BookAuthor(author_id,book_id) VALUES(?,?)";
	        try (Connection conn = DriverManager.getConnection(url);PreparedStatement stmt=conn.prepareStatement(sql)) {
	        	stmt.setString(1, a);
	        	stmt.setString(2, bookId);
	        	result = stmt.execute();     
	        	System.out.print("asdasd-- " + result );
	        } catch (SQLException e) {
	            System.out.println(e.getMessage());
	            e.printStackTrace();
	        } 
		}
		 
	        return result;	
	        }

	public List<Author> findAuthorByBookId(String id) {
		List<Author> authors = Collections.emptyList();
		ResultSet result = null;


		String sql ="select A.* from BookAuthor BA Inner Join author A ON BA.author_id=A.id where book_id = ?";

		try (Connection conn = DriverManager.getConnection(url);PreparedStatement stmt=conn.prepareStatement(sql)) {
			stmt.setString(1, id);
			result = stmt.executeQuery();
			if (result != null) {
				authors = new ArrayList<>();
			}

			Author author;
			while (result.next()) {
				author = new Author(result.getString(1),result.getString(2),result.getString(3));
				authors.add(author);
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}

		return authors;
	}

	public List<BookCopies> findBookCopiesByBookId(Books book) {
		// TODO Auto-generated method stub
		List<BookCopies> bookCopiesList = Collections.emptyList();
		ResultSet result = null;


		String sql ="select * from BookCopy  where book_id = ?";

		try (Connection conn = DriverManager.getConnection(url);PreparedStatement stmt=conn.prepareStatement(sql)) {
			stmt.setString(1, book.getId());
			result = stmt.executeQuery();
			if (result != null) {
				bookCopiesList = new ArrayList<>();
			}

			BookCopies bookCopies;
			while (result.next()) {
				bookCopies = new BookCopies(book);
				bookCopies.setAvailabilty(result.getBoolean("availability"));
				bookCopiesList.add(bookCopies);
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}

		return bookCopiesList;
		}

	public boolean UpdateBook(Books book, String previousIsbn) {
		 String sql = "UPDATE Bookinfor SET id = ?,title = ?,isbn = ?,maxBorrowDays=? WHERE id = ?";
	        boolean result = false;
	        try (Connection conn = DriverManager.getConnection(url);PreparedStatement stmt=conn.prepareStatement(sql)) {
	        	stmt.setString(1, book.getId());
	        	stmt.setString(2, book.getTitle());
	        	stmt.setString(3, book.getIsbn());
	        	stmt.setString(4, String.valueOf(book.getMaxBorrowDays()));
	        	stmt.setString(5, previousIsbn);
	        	result = stmt.execute();        	      	
	        } catch (SQLException e) {
	            System.out.println("update----->> "+e.getMessage());
	            e.printStackTrace();
	        } 
	        return result;
	}


	public boolean deleteAuthor(String previousIsbn) {
		 String sql = "Delete From BookAuthor where book_id = ? ;";
	        boolean result = false;
	        try (Connection conn = DriverManager.getConnection(url);PreparedStatement stmt=conn.prepareStatement(sql)) {
	        	stmt.setString(1, previousIsbn);
	        	result = stmt.execute();        	      	
	        } catch (SQLException e) {
	            System.out.println("delete----->> "+e.getMessage());
	            e.printStackTrace();
	        } 
	        return result;
	}
	public boolean deleteCopies(String previousIsbn) {
		 String sql = "Delete From BookCopy where book_id = ? ;";
	        boolean result = false;
	        try (Connection conn = DriverManager.getConnection(url);PreparedStatement stmt=conn.prepareStatement(sql)) {
	        	stmt.setString(1, previousIsbn);
	        	result = stmt.execute();        	      	
	        } catch (SQLException e) {
	            System.out.println("delete----->> "+e.getMessage());
	            e.printStackTrace();
	        } 
	        return result;
	}

	public List<Books> findAllBooks() {
		List<Books> books = Collections.emptyList();
		ResultSet result = null;


		String sql ="select BI.*,COUNT(BC.id) as available from Bookinfor BI INNER join BookCopy BC on BC.book_id=BI.id where BC.availability=1 GROUP BY BC.book_id ";

		try (Connection conn = DriverManager.getConnection(url);PreparedStatement stmt=conn.prepareStatement(sql)) {
			result = stmt.executeQuery();
			if (result != null) {
				books = new ArrayList<>();
			}

			Books book;
			while (result.next()) {
				book = new Books();
				book.setId(result.getString(5));
				book.setTitle(result.getString(2));
				book.setIsbn(result.getString(3));
				book.setMaxBorrowDays(Integer.parseInt(result.getString(4)));
				books.add(book);
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}

		return books;
		
	}

}
