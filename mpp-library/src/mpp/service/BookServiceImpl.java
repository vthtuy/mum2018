package mpp.service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Logger;

import mpp.Constances.Role;
import mpp.entity.Author;
import mpp.entity.BookCopies;
import mpp.entity.BookInforModel;
import mpp.entity.EmployeeModel;
import mpp.entity.UserModel;

public class BookServiceImpl {

	@SuppressWarnings("unused")
	private static final Logger LOGGER = Logger.getLogger(BookServiceImpl.class.getName());

	private static StringBuilder SEARCH_BOOK_DYNAMIC_QUERY = new StringBuilder(
			"select * from Bookinfor  where  isbn like ? or  title like ? ");

	private static StringBuilder SEARCH_AUTHORS_OF_BOOK = new StringBuilder(
			"select author.*, BookAuthor.book_id from BookAuthor, author  where BookAuthor.author_id = author.id AND BookAuthor.book_id like ? ");
	// private static StringBuilder SEARCH_BOOK_DYNAMIC_QUERY = new
	// StringBuilder("select books.*, author.firstname, author.lastname from Books
	// left join BookAuthor on books.id = bookauthor.book_id left join author on
	// author.id = bookauthor.author_id where books.isbn like ? or books.title like
	// ? ");

	// "select books.*, author.firstname, author.lastname from Books left join
	// BookAuthor on books.id = bookauthor.book_id left join author on author.id =
	// bookauthor.author_id where book.isbn like ? or book.title like ? "

	private DataAccessImpl dataAccess = new DataAccessImpl();

	public List<BookInforModel> search(String keyword, String title) {

		try (Connection conn = DriverManager.getConnection(DataAccessImpl.url);
				PreparedStatement pstmt = conn.prepareStatement(SEARCH_BOOK_DYNAMIC_QUERY.toString());) {

			pstmt.setString(1, "%" + keyword + "%");
			pstmt.setString(2, "%" + keyword + "%");

			// ResultSet resultSet = dataAccess.findFlexibleStatement("", pstmt);
			ResultSet resultSet = pstmt.executeQuery();

			List<BookInforModel> results = new ArrayList<>();

			while (resultSet.next()) {

				BookInforModel model = new BookInforModel();
				model.setId(resultSet.getString("id"));
				model.setIsbn(resultSet.getString("isbn"));
				// model.setAvaibility(resultSet.getString("avaibility"));
				model.setMaxBorrowDays(resultSet.getInt("maxBorrowDays"));

				model.setTitle(resultSet.getString("title"));

				results.add(model);
			}

			return results;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return Collections.emptyList();
	}

	public List<Author> getAuthorsByBookId(String bookid) {

		try (Connection conn = DriverManager.getConnection(DataAccessImpl.url);
				PreparedStatement pstmt = conn.prepareStatement(SEARCH_AUTHORS_OF_BOOK.toString());) {

			pstmt.setString(1, bookid);

			// ResultSet resultSet = dataAccess.findFlexibleStatement("", pstmt);
			ResultSet resultSet = pstmt.executeQuery();

			List<Author> results = new ArrayList<>();

			while (resultSet.next()) {

				Author model = new Author();
				model.setId(resultSet.getString("id"));
				model.setFirstName(resultSet.getString("firstname"));
				model.setLastName(resultSet.getString("lastname"));
				model.setPhone(resultSet.getString("phone"));
				model.setBio(resultSet.getString("bio"));
				results.add(model);
			}

			return results;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return Collections.emptyList();
	}

	public EmployeeModel login(String user, String password) {
		List<EmployeeModel> results = dataAccess.findEmployeeByIdPassword(user, password);
		EmployeeModel model = !results.isEmpty() ? results.get(0) : null;
		if (model != null) {
			Role roleEnum = Role.LIBRARIAN;
			System.out.println("model.getRole(): " + model.getRole());
			if ("1".equals(model.getRole())) {
				roleEnum = Role.ADMIN;
			} else if ("3".equals(model.getRole())) {
				roleEnum = Role.BOTH;
			}
			model.setRoleEnum(roleEnum);
		}
		return model;
	}

	public String getAvailabilityByBookId(String bookId) {
		return dataAccess
				.checkTable("select count(*) from BookCopy where book_id = '" + bookId + "' and availability is true");
	}
	
	public List<BookCopies> getBookCopybyBookId(String bookid) {

		try (Connection conn = DriverManager.getConnection(DataAccessImpl.url);
				PreparedStatement pstmt = conn.prepareStatement("select * from BookCopy where book_id like ? LIMIT 3 ");) {

			pstmt.setString(1, bookid);

			// ResultSet resultSet = dataAccess.findFlexibleStatement("", pstmt);
			ResultSet resultSet = pstmt.executeQuery();

			List<BookCopies> results = new ArrayList<>();

			while (resultSet.next()) {

				BookCopies model = new BookCopies();
				model.setId(resultSet.getInt("id"));
				model.setAvailabilty(resultSet.getBoolean("availability"));

				results.add(model);
			}

			return results;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return Collections.emptyList();
	}
}
