package mpp.service;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Logger;

import mpp.Constances;

public class InitialDatabase {

	private static final Logger LOGGER = Logger.getLogger(DataAccessImpl.class.getName());

	private static final String DATABASE_URL = Constances.DATABASE_URL; //  "jdbc:sqlite:" + "data/library.db"
	private static final String SQL_PATH = "data/sql.sql";

	private static final String DATA_INSERT_PATH = "data/data.sql";

	private Connection connect() {
		Connection conn = null;
		try {
			conn = DriverManager.getConnection(DATABASE_URL);
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return conn;
	}

	/**
	 * Insert a new row into the warehouses table
	 *
	 * @param name
	 * @param capacity
	 */
	public void insert(String name, double capacity) {
		String sql = "INSERT INTO employee(id,  firstname,  lastname, phone , account , password , role  ) VALUES\n"
				+ "(\"admin\", \"Admin\", \"Admin\", \"0433343\", \"\", \"123\", 1)";

		try (Connection conn = this.connect(); PreparedStatement pstmt = conn.prepareStatement(sql)) {
			// pstmt.setString(1, name);
			// pstmt.setDouble(2, capacity);
			pstmt.executeUpdate(sql);
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
	}

	/*
	 * create library database
	 */
	public static void createNewDatabase() {

		// String url = "jdbc:sqlite:" + "data/library.db";

		try (Connection conn = DriverManager.getConnection(DATABASE_URL)) {
			if (conn != null) {
				DatabaseMetaData meta = conn.getMetaData();
				System.out.println("The driver name is " + meta.getDriverName());
				System.out.println("A new database has been created.");
			}

		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
	}

	/**
	 * Create a new table from file
	 * 
	 * @throws IOException
	 *
	 */
	public static void createNewTable() throws IOException {

		// SQL statement for creating a new table
		StringBuilder sql = new StringBuilder();

		byte[] encoded = Files.readAllBytes(Paths.get(SQL_PATH));
		String createTableSql = new String(encoded, StandardCharsets.UTF_8);
		// StringBuilder sql = new StringBuilder(encoded);

		String[] array = createTableSql.split(";");

		sql.append(createTableSql);
		
		try (Connection conn = DriverManager.getConnection(DATABASE_URL); Statement stmt = conn.createStatement()) {
			// create a new tables
			for (int i = 0; i < array.length; i++) {
				System.out.println(array[i]);

				stmt.execute(array[i].toString());
			}
			conn.commit();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static void insertDataFromFile() throws IOException {

		// SQL statement for creating a new table
		StringBuilder sql = new StringBuilder();

		byte[] encoded = Files.readAllBytes(Paths.get(DATA_INSERT_PATH));
		String dataSql = new String(encoded, StandardCharsets.UTF_8);
		// StringBuilder sql = new StringBuilder(encoded);

		LOGGER.info("====== INSERT DATA =============");

		sql.append(dataSql);
		System.out.println(dataSql);

		try (Connection conn = DriverManager.getConnection(DATABASE_URL); Statement stmt = conn.createStatement()) {
			// create a new tables
			stmt.executeUpdate(sql.toString());
			// conn.commit();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * @param args the command line arguments
	 */
	public static void main(String[] args) {
		createNewDatabase();

		try {
			createNewTable();
			insertDataFromFile();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		//
		InitialDatabase app = new InitialDatabase();
		// insert three new rows
		// app.insert("Raw Materials", 3000);
		// app.insert("Semifinished Goods", 4000);
		// app.insert("Finished Goods", 5000);
	}
}
