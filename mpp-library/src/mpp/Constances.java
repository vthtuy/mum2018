package mpp;

public class Constances {
	
	public static final int N_800 = 1000;
	public static final int N_600 = 600;
	
	public static final String DATABASE_URL = "jdbc:sqlite:" + "data/library.db";
	
	public static final String VIEW_PATH = "/mpp/view/";
	
	public static final String VIEW_MAIN_UI = VIEW_PATH + "MainUI.fxml";
	
	public static final String VIEW_LOGIN_UI = VIEW_PATH + "LoginUI.fxml";
	
	public static final String VIEW_MEMBER_UI = VIEW_PATH + "MemberUI.fxml";
	
	public static final String VIEW_LOGIN_SUCCESSFULL_UI = VIEW_PATH + "LoginSuccessfullUI.fxml";
	
	public static final String VIEW_ADMIN_SEARCH_BOOK_UI = VIEW_PATH + "AdminSearchBookIU.fxml";

	public static final String VIEW_MAIN_BOOK = VIEW_PATH + "MainBookUI.fxml";

	public static final String VIEW_ADD_BOOK = VIEW_PATH + "AddBook.fxml";
	
	public static final String VIEW_SEARCH_BOOK = VIEW_PATH + "searchBook.fxml";
	
	public static final String VIEW_BOOK_LIST = VIEW_PATH + "BookList.fxml";

	public static final String VIEW_ADD_AUTHOR = VIEW_PATH + "addAuthor.fxml";

	public static final String CHECKOUT_BOOK_UI = VIEW_PATH + "BookCheckout.fxml";

	
	public static enum Role {
		LIBRARIAN("0"), ADMIN("1"), BOTH("3");
		
		private final String level;
	    public String getLevel() {
			return level;
		}
		private Role(String levelCode) {
	        this.level = levelCode;
	    }
	}

}
