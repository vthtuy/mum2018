package mpp.controller;

import mpp.entity.CheckoutModel;

public class Checkout {
	CheckoutModel checkModel; 
	
	public Checkout() {
		checkModel = new CheckoutModel();
	}
	
	public String CheckAll(String memId, String bookId) {
		
		try {

			
			if ( checkModel.Checkmember(memId).equals("0") ) {
				return "No member";
				};
			
			if ( checkModel.Checkbook(bookId).equals("0") ) {
				return "No book";
				};			
				
			
			if(checkModel.decreaseBook(bookId) == 1) {
				
				String max = checkModel.getMaxDays(bookId);
				checkModel.InsertCheckout(memId, bookId, max);
			} 	
		
		}catch (Exception e) {
			
			System.out.println(e.toString());
			return "Can't Find ...";
			
		}
		
		return "Successfully checked out";
		
	}  
}
