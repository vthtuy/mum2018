package mpp.controller;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Logger;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;

import javafx.scene.input.MouseEvent;
import mpp.Constances;
import mpp.entity.Author;
import mpp.entity.BookCopies;
import javafx.scene.control.Button;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Label;
import mpp.entity.BookInforModel;
import mpp.entity.MemberModel;
import mpp.entity.UserModel;
import mpp.service.BookServiceImpl;

public class AdminSearchBookController extends CommonLoginedController implements Initializable {

	private static final Logger LOGGER = Logger.getLogger(AdminSearchBookController.class.getName());

	private BookServiceImpl service = new BookServiceImpl();

	@FXML
	private TextField txtKeyword;
	@FXML
	private TextField txtTitle;
	@FXML
	private Button tbnSubmit;

	@FXML
	private TableView<BookInforModel> tableView;
	@FXML
	private Label lblAuthorDetail;
	@FXML
	private Label lblAuthors;
	@FXML
	private Label tblBookCopy;
	@FXML
	private Label tblBookCopyInfor;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// 
		lblAuthorDetail.setVisible(false);
		lblAuthors.setVisible(false);
		tblBookCopy.setVisible(false);
		tblBookCopyInfor.setVisible(false);
		// get all data
		List<BookInforModel> results = service.search("", "");

		populateBooks(results);

		ObservableList data = FXCollections.observableList(results);

		tableView.getItems().setAll(data);
	}

	@FXML
	protected void handleSearchAction() {
		// search by key only
		List<BookInforModel> results = service.search(txtKeyword.getText(), "");

		LOGGER.info(results.size() + "");
		populateBooks(results);

		ObservableList data = FXCollections.observableList(results);

		// set data to show result list
		tableView.getItems().setAll(data);
	}

	private void populateBooks(List<BookInforModel> results) {
		for (BookInforModel book : results) {
			List<Author> authors = service.getAuthorsByBookId(book.getId());
			String setAuthorsInfor = "";
			String setAuthorsBio = "";
			// Author information
			for (int i = 0; i < authors.size(); i++) {
				Author author = authors.get(i);
				setAuthorsInfor = setAuthorsInfor + " " + author.getFirstName() + " " + author.getLastName();
				setAuthorsBio = setAuthorsBio + " " + author.getFirstName() + ": "
						+ (author.getBio() != null ? author.getBio() : "") ;
				if (i < authors.size() - 1) {
					setAuthorsInfor = setAuthorsInfor + "; ";
					setAuthorsBio = setAuthorsBio + "; ";
				}
			}
			book.setAuthorsInfor(setAuthorsInfor);
			book.setAuthorsAddresses(setAuthorsBio);
			
			// book availability from BookCopy
			book.setAvaibility(service.getAvailabilityByBookId(book.getId()));
		}
	}

	@FXML
	public void displaySelectedItem(MouseEvent event) {
		lblAuthorDetail.setVisible(true);
		lblAuthors.setVisible(true);
		tblBookCopy.setVisible(true);
		tblBookCopyInfor.setVisible(true);
		
		BookInforModel item = tableView.getSelectionModel().getSelectedItem();
		lblAuthors.setText(item.getAuthorsAddresses());

		List<BookCopies> copies = service.getBookCopybyBookId(item.getId());
		String copyCodes = "";
		for (int i = 0; i < copies.size(); i++) {
			BookCopies copy = copies.get(i);
			copyCodes = copyCodes + copy.getId();
			if (i < copies.size() - 1) {
				copyCodes = copyCodes + "; ";
			}
		}
		tblBookCopyInfor.setText(copyCodes);
	}

}
