package mpp.controller;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Logger;

import javafx.event.*;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;

public class LoginSuccessfulController extends CommonLoginedController implements Initializable {
	
	private static final Logger logger = Logger.getLogger(LoginSuccessfulController.class.getName());
	
	@FXML
	private Label lblWelcomeUser;

	public void showWelComeMessage() {
		logger.info("user: " + getLoginedUser().getText());
		
		lblWelcomeUser.setVisible(true);
		lblWelcomeUser.setText("Welcome \"" + getLoginedUser().getText() + "\" to Vitamin Library");
		
	}
	
	public void initSessionID(final FlowManager flowManager, String sessionID) {
		//sessionLabel.setText(sessionID);
	//	System.out.println("sessionID: " + sessionID);
//		logoutButton.setOnAction(new EventHandler<ActionEvent>() {
//			@Override
//			public void handle(ActionEvent event) {
//				flowManager.logout();
//			}
//		});
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		
		//MenuAreaGenerator menuGenerator = new MenuAreaGenerator();
		//MenuBar mainMenu = menuGenerator.generateMenuByRole(null);

		//topContainer.getChildren().add(mainMenu);
	}
}
