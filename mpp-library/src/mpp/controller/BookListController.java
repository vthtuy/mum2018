package mpp.controller;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import mpp.entity.Books;
import mpp.service.DataAccessImpl;

public class BookListController implements Initializable{
	
    @FXML
    private TableView<Books> tableMember;
    @FXML
    private TableColumn celId;

    @FXML
    private TableColumn celTitle;

    @FXML
    private TableColumn celIsbn;

    @FXML
    private TableColumn celTime;

    @FXML
    private TableColumn celAvail;
    
    
	private DataAccessImpl dataAccess = new DataAccessImpl();
    
	 private ObservableList<Books> getInitialTableData() {        
	        List<Books> members = dataAccess.findAllBooks();  
	        System.out.println("members " + members.size());
	        ObservableList<Books> data = FXCollections.observableList(members);
	        return data;
	    }
	 
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		// TODO Auto-generated method stub
		celId.setCellValueFactory(new PropertyValueFactory("isbn"));
		celTitle.setCellValueFactory(new PropertyValueFactory("title"));
		celIsbn.setCellValueFactory(new PropertyValueFactory("isbn"));
		celTime.setCellValueFactory(new PropertyValueFactory("maxBorrowDays"));
		celAvail.setCellValueFactory(new PropertyValueFactory("id"));

        tableMember.getItems().setAll(getInitialTableData());
	}

}
