package mpp.controller;

import java.io.IOException;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.MenuBar;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import mpp.Constances;
import mpp.Constances.Role;
import mpp.entity.EmployeeModel;
import mpp.service.DataAccessImpl;

public class LoginController {

	@FXML
	private TextField txtId;
	@FXML
	private TextField txtPwd;
	@FXML
	private Button tbnLogin;
	@FXML
	private Label lblMessage;

	private FlowManager loginManager;

	private DataAccessImpl dataAccess = new DataAccessImpl();

	public void initialize() {
		// do nothing
	}

	/**
	 * Check authorization credentials.
	 * 
	 * If accepted, return a sessionID for the authorized session otherwise, return
	 * null.
	 */
	private String authorize(EmployeeModel login ) {

		return login != null ? generateSessionID() : null;
	}

	private EmployeeModel login() {
		List<EmployeeModel> results = dataAccess.findEmployeeByIdPassword(txtId.getText(), txtPwd.getText());
		EmployeeModel model = !results.isEmpty() ? results.get(0) : null;
		if (model != null) {
			Role roleEnum = Role.LIBRARIAN;
			System.out.println("model.getRole(): " + model.getRole());
			if ("1".equals(model.getRole())) {
				roleEnum = Role.ADMIN;
			} else if ("3".equals(model.getRole())) {
				roleEnum = Role.BOTH;
			}
			model.setRoleEnum(roleEnum);
		}
		return model;
	}

	private static int sessionID = 0;

	private String generateSessionID() {
		sessionID++;
		return "usersession" + txtId.getText();
	}

//	private void showMainView(String sessionID) {
//	    try {
//	      FXMLLoader loader = new FXMLLoader(
//	        getClass().getResource("mainview.fxml")
//	      );
//	      scene.setRoot((Parent) loader.load());
//	      MainViewController controller = 
//	        loader.<MainViewController>getController();
//	      controller.initSessionID(this, sessionID);
//	    } catch (IOException ex) {
//	      Logger.getLogger(LoginManager.class.getName()).log(Level.SEVERE, null, ex);
//	    }
//	  }

	@FXML
	protected void handleLoginAction() {
		System.out.println("In #handleLoginAction");

		EmployeeModel model = login();
		String sessionID = authorize(model);
		 
		if (sessionID != null) {
			//
			// loginManager.authenticated(txtId.getText());

			System.out.println("Signed In sessionID :  " + sessionID);
			lblMessage.setText("Welcome to Vitamin Library");
			lblMessage.setStyle("visibility: visible ");
			lblMessage.setVisible(false);

			showLoginSucessfulStageWindow(model);

		} else {
			lblMessage.setText("User or Password is not correct");
			lblMessage.setVisible(true);
		}

	}

	private void showLoginSucessfulStageWindow(EmployeeModel model) {
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource(Constances.VIEW_LOGIN_SUCCESSFULL_UI));

//			scene.setRoot((Parent) loader.load());
//			MainViewController controller = loader.<MainViewController>getController();
//			controller.initSessionID(this, sessionID);

			VBox topContainer = new VBox();
			MenuAreaGenerator menuGenerator = new MenuAreaGenerator();
			// generate menu by User Role
			MenuBar mainMenu = menuGenerator.generateMenuByRole(model.getRoleEnum());
			topContainer.getChildren().add(mainMenu);

			// load() -> intialize() in Controller will be called
			Parent loginedSuccessfullUI = (Parent) loader.load();
			topContainer.getChildren().add(loginedSuccessfullUI);

			txtId.getScene().setRoot(topContainer);

			LoginSuccessfulController controller = loader.<LoginSuccessfulController>getController();
			// pass value from this Controller to another Controller
			controller.getLoginedUser().setText(txtId.getText());
			// store data to reuse
			CommonLoginedController.staticLoginedUser = txtId.getText();
			CommonLoginedController.staticLoginedUserRole = StringUtils.isNotEmpty(model.getRole()) ? model.getRole().trim() : model.getRole();
			
			controller.showWelComeMessage();
			
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}

}
