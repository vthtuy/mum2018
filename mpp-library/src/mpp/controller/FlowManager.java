package mpp.controller;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.MenuBar;
import javafx.scene.layout.VBox;
import mpp.Constances;
import mpp.entity.EmployeeModel;
import mpp.service.EmployeeServiceImpl;

public class FlowManager {

	private String user;

	private Scene scene;
	
	private EmployeeServiceImpl service = new EmployeeServiceImpl();

	public FlowManager(Scene scene) {
		this.scene = scene;
	}

	public void showMainScreen() {
		try {

			FXMLLoader loader = new FXMLLoader(getClass().getResource(Constances.VIEW_MAIN_UI));
			
			VBox topContainer = new VBox();
			MenuAreaGenerator menuGenerator = new MenuAreaGenerator();
			//
			MenuBar mainMenu = menuGenerator.generateMenuByRole(null);
			topContainer.getChildren().add(mainMenu);

			// load() -> intialize() in Controller will be called
			Parent newUI = (Parent) loader.load();
			topContainer.getChildren().add(newUI);

			scene.setRoot(topContainer);
			// LoginController controller = loader.<LoginController>getController();
			// controller.initManager(this);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public void showAddBook() {
		try {

			FXMLLoader loader = new FXMLLoader(getClass().getResource(Constances.VIEW_MAIN_BOOK));

			VBox topContainer = new VBox();
			MenuAreaGenerator menuGenerator = new MenuAreaGenerator();
			// TODO ------
			MenuBar mainMenu = menuGenerator.generateMenuByRole(getRoleAfterLogined());
			topContainer.getChildren().add(mainMenu);

			// load() -> intialize() in Controller will be called
			Parent newUI = (Parent) loader.load();
			topContainer.getChildren().add(newUI);

			scene.setRoot(topContainer);

			// LoginController controller = loader.<LoginController>getController();
			// controller.initManager(this);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
	public void showScreen(String screenId, String user) {

		try {

			FXMLLoader loader = new FXMLLoader(getClass().getResource(screenId));
			
			VBox topContainer = new VBox();
			MenuAreaGenerator menuGenerator = new MenuAreaGenerator();			
			
			///service
			EmployeeModel model = service.getRoleById2(user);
			
			MenuBar mainMenu = menuGenerator.generateMenuByRole(model.getRoleEnum());
			topContainer.getChildren().add(mainMenu);

			// load() -> intialize() in Controller will be called
			Parent newUI = (Parent) loader.load();
			topContainer.getChildren().add(newUI);

			scene.setRoot(topContainer);			
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
	
	public void showCheckoutBookScreen() {
		try {

			FXMLLoader loader = new FXMLLoader(getClass().getResource(Constances.CHECKOUT_BOOK_UI));
			
			VBox topContainer = new VBox();
			MenuAreaGenerator menuGenerator = new MenuAreaGenerator();
			 
			System.err.println(" Need Logined ID showCheckoutBookScreen !!!!!!!!" + user);
			
			EmployeeServiceImpl service = new EmployeeServiceImpl();
			
			///service
			// EmployeeModel model = service.getRoleById(user);
			
			
			MenuBar mainMenu = menuGenerator.generateMenuByRole(getRoleAfterLogined());
			topContainer.getChildren().add(mainMenu);

			// load() -> intialize() in Controller will be called
			Parent newUI = (Parent) loader.load();
			topContainer.getChildren().add(newUI);

			scene.setRoot(topContainer);
			
			// LoginController controller = loader.<LoginController>getController();
			// controller.initManager(this);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	public void showAdminSearchBookScreen(String user) {

		try {

			FXMLLoader loader = new FXMLLoader(getClass().getResource(Constances.VIEW_ADMIN_SEARCH_BOOK_UI));
			
			VBox topContainer = new VBox();
			MenuAreaGenerator menuGenerator = new MenuAreaGenerator();
			//TODO ------ 
			System.out.println("User: " + CommonLoginedController.staticLoginedUser);
			
			EmployeeServiceImpl service = new EmployeeServiceImpl();
			
			///service
			EmployeeModel model = service.getRoleById2(user);
			
			MenuBar mainMenu = menuGenerator.generateMenuByRole(model.getRoleEnum());
			topContainer.getChildren().add(mainMenu);

			// load() -> intialize() in Controller will be called
			Parent newUI = (Parent) loader.load();
			topContainer.getChildren().add(newUI);

			scene.setRoot(topContainer);
			
			// LoginController controller = loader.<LoginController>getController();
			// controller.initManager(this);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
	public void showLoginScreen() {
		try {

			FXMLLoader loader = new FXMLLoader(getClass().getResource(Constances.VIEW_LOGIN_UI));
			
			VBox topContainer = new VBox();
			MenuAreaGenerator menuGenerator = new MenuAreaGenerator();
			//
			MenuBar mainMenu = menuGenerator.generateMenuByRole(null);
			topContainer.getChildren().add(mainMenu);

			// load() -> intialize() in Controller will be called
			Parent newUI = (Parent) loader.load();
			topContainer.getChildren().add(newUI);

			scene.setRoot(topContainer);
			// LoginController controller = loader.<LoginController>getController();
			// controller.initManager(this);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
	private Constances.Role getRoleAfterLogined() {
		System.out.println(" Role : " + CommonLoginedController.staticLoginedUserRole);
		return convertRoleToEnum(CommonLoginedController.staticLoginedUserRole);
	}
	
	private Constances.Role convertRoleToEnum(String role) {
		Constances.Role result = Constances.Role.LIBRARIAN;
		
		if ("3".equals(CommonLoginedController.staticLoginedUserRole)) {
			result = Constances.Role.BOTH;
		}
		if ("1".equals(CommonLoginedController.staticLoginedUserRole)) {
			result = Constances.Role.ADMIN;
		} 
		return result;
	}

	public void authenticated(String sessionID) {
		this.user = sessionID;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public void logout() {
		// TODO Auto-generated method stub

	}

}
