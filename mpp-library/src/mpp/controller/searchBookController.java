package mpp.controller;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;
import mpp.entity.Author;
import mpp.entity.BookCopies;
import mpp.entity.Books;
import mpp.service.DataAccessImpl;

public class searchBookController implements Initializable{
	private DataAccessImpl dataAccess = new DataAccessImpl();
	
	@FXML
    private GridPane addBookUI;

    @FXML
    private TextField title;

    @FXML
    private TextField isbn;

    @FXML
    private TextField copies;

    @FXML
    private ListView<Author> authorListView;

    @FXML
    private TextField maxDays;

    @FXML
    private Button search;

    @FXML
    private Text searchtxt;

    @FXML
    private TextField searchtext;

    @FXML
    private Button editSave;
    
    static String previousIsbn;
    @FXML
    private Text successText;
    
    @FXML
    void addAuthor(ActionEvent event) {

    }
    
    @FXML
    void searchButton(MouseEvent event) {
    	try {
    		previousIsbn=searchtext.getText();
    	Books book=dataAccess.findBookByIsbn(searchtext.getText());
    	List<Author> authorList = dataAccess.findAuthorByBookId(book.getId());
    	List<BookCopies> bookCopieslist =dataAccess.findBookCopiesByBookId(book);
    	title.setText(book.getTitle());
    	isbn.setText(book.getIsbn());
    	copies.setText(String.valueOf(bookCopieslist.size()));
    	maxDays.setText(String.valueOf(book.getMaxBorrowDays()));
    	for(Author author: authorList) {
    		authorListView.getSelectionModel().select(Integer.parseInt(author.getId())-1);
    		}
    	enableDisableField(false);
    	}
    	catch(Exception ex) {
    		System.out.println(ex);
        	enableDisableField(true);
    	}
    }
    
    void enableDisableField(Boolean bool) {
    	title.setDisable(bool);
    	isbn.setDisable(bool);
    	copies.setDisable(bool);
    	maxDays.setDisable(bool);
    	authorListView.setDisable(bool);
    	editSave.setDisable(bool);
    }
    
    @FXML
    void saveBook(MouseEvent event) {
    	try {
    	ArrayList<String> authorIdList= new ArrayList<String>();
    	List<Author> authorList =authorListView.getSelectionModel().getSelectedItems();
    	for (Author a : authorList) {
    		authorIdList.add(a.getId());
    	}
    	Books book = new Books();
    	book.setId(isbn.getText());
    	book.setTitle(title.getText());
    	book.setIsbn(isbn.getText());
    	book.setMaxBorrowDays(Integer.parseInt(maxDays.getText()));
    	boolean saveBook = dataAccess.UpdateBook(book,previousIsbn);
    	boolean deleteAuthor =  dataAccess.deleteAuthor(previousIsbn);
    	boolean saveAuthor = dataAccess.saveAuthor(authorIdList,book.getId());
    	boolean deleteCopies =  dataAccess.deleteCopies(previousIsbn);

    	List<BookCopies> copiesList = new ArrayList();
    	boolean saveCopies=false;
    	for(int i = 0;i<Integer.parseInt(copies.getText());i++){
    		copiesList.add(new BookCopies(book));
    		saveCopies = dataAccess.saveCopies(book);
    		}
    	successText.setText("Success");
    	}
    	catch(Exception ex) {
        	successText.setText("Failed");
    		System.out.println(ex);
        	enableDisableField(true);
    	}
    	clearField();
    }
    
    
    
	private void clearField() {
		title.clear();
		isbn.clear();
		copies.clear();
		authorListView.getSelectionModel().clearSelection();
		maxDays.clear();
		searchtext.clear();
    	enableDisableField(true);
	}

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		enableDisableField(true);
		ObservableList<Author> options = FXCollections.observableArrayList(
				new Author("1","First","Author"),			
				new Author("2","Second","Author"),
				new Author("3","Third","Author"),			
				new Author("4","Fourth","Author"),			
				new Author("5","Fifth","Author"),			
				new Author("6","Sixth","Author"),			
				new Author("7","Seventh","Author"),			
				new Author("8","Eighth","Author"),			
				new Author("9","Nineth","Author")			
				);
		authorListView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
		authorListView.setItems(options);
		authorListView.setCellFactory(param -> new ListCell<Author>() {
		    @Override
		    protected void updateItem(Author item, boolean empty) {
		        super.updateItem(item, empty);

		        if (empty || item == null || item.toString() == null) {
		            setText(null);
		        } else {
		            setText(item.toString());
		        }
		    }
		});
		
	}

}
