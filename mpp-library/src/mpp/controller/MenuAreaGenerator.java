package mpp.controller;

import java.util.ArrayList;
import java.util.List;

import application.MemberWindow;
import javafx.collections.FXCollections;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCombination;
import mpp.Constances;

public class MenuAreaGenerator {
	
	private static final int SIZE = 25;

	private Constances.Role role;

	public MenuAreaGenerator() {
		// do nothing
	}

	public MenuBar generateMenuByRole(Constances.Role role) {
		// Create MenuBar
		MenuBar menuBar = new MenuBar();

		// Create menus
		Menu libMenu = new Menu("Librarian");
		Menu adminMenu = new Menu("Administrator");
		Menu otherMenu = new Menu("Menu");

		// Create MenuItems
		MenuItem checkoutItem = new MenuItem("Checkout Book");
		// MenuItem libSearchBookItem = new MenuItem("Search Book");

		MenuItem addBookItem = new MenuItem("Manage Book");
		MenuItem menuMemberMangement = new MenuItem("Manage Member");
		MenuItem adminSearchBookItem = new MenuItem("Search Book");
		MenuItem searchBookItem = new MenuItem("Search Book");
		MenuItem loginItem = new MenuItem("Sign in");
		MenuItem logoutItem = new MenuItem("Sign out");
		
		Image signoutIcon = ImageUtil.getImage("/application/image/sign_out.png");
		ImageView iv2 = new ImageView();
        iv2.setImage(signoutIcon);
        iv2.setFitWidth(SIZE);
        iv2.setFitHeight(SIZE);
		logoutItem.setGraphic(iv2);
		
		Image bookIcon = ImageUtil.getImage("/application/image/book.png");
		ImageView iv3 = new ImageView();
		iv3.setImage(bookIcon);
		iv3.setFitWidth(SIZE);
		iv3.setFitHeight(SIZE);
        addBookItem.setGraphic(iv3);
        
        Image memberIcon = ImageUtil.getImage("/application/image/member.png");
		ImageView iv4 = new ImageView();
		iv4.setImage(memberIcon);
		iv4.setFitWidth(SIZE);
		iv4.setFitHeight(SIZE);
		menuMemberMangement.setGraphic(iv4);
		
		Image searchIcon = ImageUtil.getImage("/application/image/search.png");
		ImageView iv5 = new ImageView();
		iv5.setImage(searchIcon);
		iv5.setFitWidth(SIZE);
		iv5.setFitHeight(SIZE);
		adminSearchBookItem.setGraphic(iv5);
		searchBookItem.setGraphic(iv5);
		
		Image checkoutIcon = ImageUtil.getImage("/application/image/checkout.png");
		ImageView iv6 = new ImageView();
		iv6.setImage(checkoutIcon);
		iv6.setFitWidth(SIZE);
		iv6.setFitHeight(SIZE);
		checkoutItem.setGraphic(iv6);
		
		Image signinIcon = ImageUtil.getImage("/application/image/sign_in.png");
		ImageView iv7 = new ImageView();
		iv7.setImage(signinIcon);
		iv7.setFitWidth(SIZE);
		iv7.setFitHeight(SIZE);
		loginItem.setGraphic(iv7);
		
		loginItem.setOnAction(evt -> {

			System.out.println("loginItem ...........");
			FlowManager flowManager = new FlowManager(menuBar.getScene());
			flowManager.showLoginScreen();

		});
		if (role == null) {
			otherMenu.getItems().addAll(loginItem);
			menuBar.getMenus().addAll(otherMenu);
			return menuBar;
		}

		// Add menuItems to the Menus
		libMenu.getItems().addAll(checkoutItem, searchBookItem);
		adminMenu.getItems().addAll(addBookItem, menuMemberMangement, adminSearchBookItem);

		otherMenu.getItems().addAll(logoutItem);

		List<Menu> menus = new ArrayList<>();

		if (Constances.Role.LIBRARIAN.equals(role) || Constances.Role.BOTH.equals(role)) {
			menus.add(libMenu);
		}
		if (Constances.Role.ADMIN.equals(role) || Constances.Role.BOTH.equals(role)) {
			menus.add(adminMenu);
		}
		menus.add(otherMenu);
		// Add Menus to the MenuBar
		menuBar.getMenus().addAll(menus);

		// Set Accelerator for Exit MenuItem.
		logoutItem.setAccelerator(KeyCombination.keyCombination("Ctrl+X"));
		logoutItem.setOnAction(evt -> {

			System.out.println("logoutItem ...........");
			FlowManager flowManager = new FlowManager(menuBar.getScene());
			CommonLoginedController.staticLoginedUser = null;
			CommonLoginedController.staticLoginedUserRole = null;
			flowManager.showLoginScreen();

		});
		
		checkoutItem.setOnAction(evt -> {

			System.out.println("checkoutItem ...........");
			FlowManager flowManager = new FlowManager(menuBar.getScene());
			flowManager.showCheckoutBookScreen();

		});
		addBookItem.setOnAction(evt ->{
			FlowManager flowManager = new FlowManager(menuBar.getScene());
			flowManager.showAddBook();
		});
		adminSearchBookItem.setOnAction(evt -> {

			System.out.println("searchBookItem ...........");
			FlowManager flowManager = new FlowManager(menuBar.getScene());
			flowManager.showAdminSearchBookScreen(CommonLoginedController.staticLoginedUser);

		});
		
		searchBookItem.setOnAction(evt -> {

			System.out.println("searchBookItem ...........");
			FlowManager flowManager = new FlowManager(menuBar.getScene());
			flowManager.showAdminSearchBookScreen(CommonLoginedController.staticLoginedUser);

		});
		
		menuMemberMangement.setOnAction(evt ->{
			FlowManager flowManager = new FlowManager(menuBar.getScene());
			flowManager.showScreen(Constances.VIEW_MEMBER_UI, CommonLoginedController.staticLoginedUser);;
		});

//        BorderPane root = new BorderPane();
//        root.setTop(menuBar);
//        Scene scene = new Scene(root, 350, 200);

		return menuBar;
	}

	public Constances.Role getRole() {
		return role;
	}

	public void setRole(Constances.Role role) {
		this.role = role;
	}

}
