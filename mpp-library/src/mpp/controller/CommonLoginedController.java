package mpp.controller;

import javafx.fxml.FXML;
import javafx.scene.control.TextField;

/**
 * Use for Logined User
 * @author 84933
 *
 */
public class CommonLoginedController {
	
	public static String staticLoginedUser;
	
	public static String staticLoginedUserRole;
	
	@FXML
	private TextField loginedUser;
	
	public TextField getLoginedUser() {
		return loginedUser;
	}

	public void setLoginedUser(TextField loginedUser) {
		this.loginedUser = loginedUser;
	}
}
