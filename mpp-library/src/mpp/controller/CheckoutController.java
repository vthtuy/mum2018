package mpp.controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

public class CheckoutController {
	
	@FXML private TextField bookId;
	@FXML private TextField memberId;
	@FXML private Label result;
	
    @FXML
    private void pressBtn(ActionEvent event){
    	
    	
        //System.out.println("hello");
        
        Checkout checkB = new Checkout();
        result.setText(  checkB.CheckAll(memberId.getText(), bookId.getText()) );
        
    }
	
}
