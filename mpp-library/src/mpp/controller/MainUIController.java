package mpp.controller;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.ComboBox;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import mpp.Constances;

public class MainUIController implements Initializable {
    @FXML
    private BorderPane borderPane;

    @FXML
    void addBook(MouseEvent event) throws IOException {
    	loadUI(Constances.VIEW_ADD_BOOK);
    }

    @FXML
    void editBook(MouseEvent event) throws IOException {
    	loadUI(Constances.VIEW_SEARCH_BOOK);
    }

    @FXML
    void home(MouseEvent event) throws IOException {
    	loadUI(Constances.VIEW_BOOK_LIST);
    }
    
    private void loadUI(String ui) throws IOException {
    	FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource(ui));     
    	Parent root = FXMLLoader.load(getClass().getResource(ui));
    	borderPane.setCenter(root);
    }
    

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		// TODO Auto-generated method stub
    	try {
			loadUI(Constances.VIEW_BOOK_LIST);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
