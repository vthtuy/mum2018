package mpp.controller;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import mpp.Constances;
import mpp.entity.Author;
import mpp.entity.BookCopies;
import mpp.entity.Books;
import mpp.entity.EmployeeModel;
import mpp.service.DataAccessImpl;

public class BookController implements Initializable{
	
	private DataAccessImpl dataAccess = new DataAccessImpl();
	
   
    @FXML
    private GridPane addBookUI;

    @FXML
    private TextField title;

    @FXML
    private TextField isbn;

    @FXML
    private TextField copies;

    @FXML
    private ListView<Author> authorListView;

    @FXML
    private TextField maxDays;
    @FXML
    private Text message;
    
    @FXML
    private Text succuessTxt;


	@FXML
    void addAuthor1(ActionEvent event) {
		
    }

	private void clearField() {
		title.clear();
		isbn.clear();
		copies.clear();
		authorListView.getSelectionModel().clearSelection();
		maxDays.clear();
	}

    @FXML
    void saveBook(MouseEvent event) {
    	try {
	    	ArrayList<String> authorIdList= new ArrayList<String>();
	    	List<Author> authorList =authorListView.getSelectionModel().getSelectedItems();
	    	for (Author a : authorList) {
	    		authorIdList.add(a.getId());
	    	}
	    	Books book = new Books();
	    	book.setId(isbn.getText());
	    	book.setTitle(title.getText());
	    	book.setIsbn(isbn.getText());
	    	book.setMaxBorrowDays(Integer.parseInt(maxDays.getText()));
	    	boolean saveBook = dataAccess.saveBook(book);
	    	boolean saveAuthor = dataAccess.saveAuthor(authorIdList,book.getId());
	    	List<BookCopies> copiesList = new ArrayList();
	    	boolean saveCopies=false;
	    	for(int i = 0;i<Integer.parseInt(copies.getText());i++){
	    		copiesList.add(new BookCopies(book));
	    		saveCopies = dataAccess.saveCopies(book);
	    	}
	    	succuessTxt.setText("Success");
    	}
    	catch(Exception ex) {
	    	succuessTxt.setText("Failed");
    		System.out.println(ex);
    	}
    	 clearField();

    }

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub

		
		ObservableList<Author> options = FXCollections.observableArrayList(
				new Author("1","First","Author"),			
				new Author("2","Second","Author"),
				new Author("3","Third","Author"),			
				new Author("4","Fourth","Author"),			
				new Author("5","Fifth","Author"),			
				new Author("6","Sixth","Author"),			
				new Author("7","Seventh","Author"),			
				new Author("8","Eighth","Author"),			
				new Author("9","Nineth","Author")
				);
		authorListView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
		authorListView.setItems(options);
		authorListView.setCellFactory(param -> new ListCell<Author>() {
		    @Override
		    protected void updateItem(Author item, boolean empty) {
		        super.updateItem(item, empty);

		        if (empty || item == null || item.toString() == null) {
		            setText(null);
		        } else {
		            setText(item.toString());
		        }
		    }
		});
	}
	
	
	
	
	   @FXML
	    void addAuthor(ActionEvent event) throws IOException {
		   Parent root = null;
	    	root = FXMLLoader.load(getClass().getResource(Constances.VIEW_ADD_AUTHOR));
	    	addBookUI.getChildren().clear();
	    	addBookUI.add(root, 0, 2);

	    }

}
