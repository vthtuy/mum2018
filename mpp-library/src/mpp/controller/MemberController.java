package mpp.controller;

import java.net.URL;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.ResourceBundle;

import org.apache.commons.lang3.StringUtils;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import mpp.entity.Address;
import mpp.entity.MemberModel;
import mpp.service.DataAccessImpl;

public class MemberController implements Initializable {

	@FXML private Label lblMessage;
	@FXML private Label lblHeader2;
	
	@FXML private TextField inputId;
	@FXML private TextField inputFirstName;
	@FXML private TextField inputLastName;
	@FXML private TextField inputStreet;
	@FXML private TextField inputCity;
	
	@FXML private TextField inputState;
	@FXML private TextField inputZip;
	@FXML private TextField inputPhoneNumber;
	
	@FXML private ListView<MemberModel> listMember;
	
	@FXML private TableView<MemberModel> tableMember;	
	
	@FXML private TableColumn colId;
	@FXML private TableColumn colFirstName;
	@FXML private TableColumn colLastName;
	
	private ObservableList<MemberModel> memberList;
	
	private DataAccessImpl dataAccess = new DataAccessImpl();
	
	private boolean isAdd = true;
	
	private MemberModel editingMember = null;
	
	private List<TextField> requiredFields;
	 @Override
    public void initialize(URL location, ResourceBundle resources) {
       
		 colId.setCellValueFactory(new PropertyValueFactory("id"));
		 colFirstName.setCellValueFactory(new PropertyValueFactory("firstName"));
		 colLastName.setCellValueFactory(new PropertyValueFactory("lastName"));
		 memberList = getInitialTableData();
        tableMember.getItems().setAll(memberList);
        
        requiredFields = Arrays.asList(inputId, inputFirstName, inputLastName);

    }
		
	
	@FXML private void submitMember()
	{
		if(!validateInput())
		{
			return;
		}
			
		MemberModel member = editingMember;
		Address address;
		if(isAdd)
		{
			member = new MemberModel();
			address = new Address();
		}
		else
		{
			address = editingMember.getAddress();
		}
		member.setId(inputId.getText());
		member.setFirstName(inputFirstName.getText());
		member.setLastName(inputLastName.getText());
		member.setPhone(inputPhoneNumber.getText());
		
		address.setStreet(inputStreet.getText());
		address.setCity(inputCity.getText());
		address.setState(inputState.getText());
		address.setZip(inputZip.getText());
		member.setAddress(address);
		boolean result = true;
		if(isAdd)
		{
			result = dataAccess.insertMember(member);
			lblMessage.setText(result? "" : "Duplicated member");
		} else
		{
			dataAccess.updateMember(member);
		}
		
		
		//refresh member list
		memberList = getInitialTableData();		
		tableMember.getItems().clear();
		tableMember.getItems().addAll(memberList);
		if(isAdd && result)
		{
			clearAddView();	
		}
	}	
	
	@FXML private void viewDetail(MouseEvent event)
	{
		editingMember = tableMember.getSelectionModel().getSelectedItem();
		if(editingMember != null)
		{
			isAdd = false;
			lblHeader2.setText("Edit Member");
			inputId.setText(editingMember.getId());
			inputFirstName.setText(editingMember.getFirstName());
			inputLastName.setText(editingMember.getLastName());
			inputPhoneNumber.setText(editingMember.getPhone());	
			
			Address address = editingMember.getAddress();
			if(address != null)
			{
				inputStreet.setText(address.getStreet());
				inputCity.setText(address.getCity());
				inputState.setText(address.getState());
				inputZip.setText(address.getZip());	
			}
		}
	}
	
	@FXML private void showAddMember()
	{
		lblHeader2.setText("Add New Member");
		clearAddView();		
	}
	
	private void clearAddView()
	{
		isAdd = true;
		inputId.clear();
		inputFirstName.clear();
		inputLastName.clear();
		inputPhoneNumber.clear();
		inputStreet.clear();
		inputCity.clear();
		inputState.clear();
		inputZip.clear();
		lblMessage.setText("");
	}
	
	 private ObservableList<MemberModel> getInitialTableData() {        
        List<MemberModel> members = dataAccess.findAllMembers();  
        System.out.println("members " + members.size());
        ObservableList<MemberModel> data = FXCollections.observableList(members);
        return data;
    }
	 
	 private boolean validateInput()
	 {
		 boolean isValid = true;
		 for (Iterator iterator = requiredFields.iterator(); iterator.hasNext();) {
			TextField input = (TextField) iterator.next();
			if(StringUtils.isEmpty(input.getText()))
			{
				isValid = false;
				lblMessage.setText("Please fill in the required field before saving changes.\nFields marked with star are required.");
			}
		}
		 return isValid;
	 }

}
