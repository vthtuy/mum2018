package mpp.entity;

import java.util.List;

public class BookInforModel {

	private String id;
	private String isbn;
	private String avaibility;
	private String title;
	private List<Author> author;
	private int maxBorrowDays;
	private List<BookCopies> copies;
	
	private String description;
	private String note;
	private String authorsInfor;
	private String authorsAddresses;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public String getAvaibility() {
		return avaibility;
	}

	public void setAvaibility(String avaibility) {
		this.avaibility = avaibility;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public List<Author> getAuthor() {
		return author;
	}

	public void setAuthor(List<Author> author) {
		this.author = author;
	}

	public int getMaxBorrowDays() {
		return maxBorrowDays;
	}

	public void setMaxBorrowDays(int maxBorrowDays) {
		this.maxBorrowDays = maxBorrowDays;
	}

	public List<BookCopies> getCopies() {
		return copies;
	}

	public void setCopies(List<BookCopies> copies) {
		this.copies = copies;
	}

	public String getAuthorsInfor() {
		return authorsInfor;
	}

	public void setAuthorsInfor(String authorsInfor) {
		this.authorsInfor = authorsInfor;
	}

	public String getAuthorsAddresses() {
		return authorsAddresses;
	}

	public void setAuthorsAddresses(String authorsAddresses) {
		this.authorsAddresses = authorsAddresses;
	}

}
