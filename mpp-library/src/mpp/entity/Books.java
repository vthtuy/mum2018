package mpp.entity;

import java.util.List;

public class Books {
	private String id;
		private String title;
		private String isbn; 
		private List<Author> author;
		private int maxBorrowDays;
		private List<BookCopies> copies;
		
		public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}

		public String getTitle() {
			return title;
		}


		public void setTitle(String title) {
			this.title = title;
		}


		public String getIsbn() {
			return isbn;
		}


		public void setIsbn(String isbn) {
			this.isbn = isbn;
		}


		public List<Author> getAuthor() {
			return author;
		}


		public void setAuthor(List<Author> author) {
			this.author = author;
		}


		public int getMaxBorrowDays() {
			return maxBorrowDays;
		}


		public void setMaxBorrowDays(int maxBorrowDays) {
			this.maxBorrowDays = maxBorrowDays;
		}


		public List<BookCopies> getCopies() {
			return copies;
		}


		public void setCopies(List<BookCopies> copies) {
			this.copies = copies;
		}




	
}
