package mpp.entity;

import java.sql.DriverManager;
import java.sql.SQLException;

import mpp.Constances;
import mpp.service.DataAccessImpl;

public class CheckoutModel {
	
	DataAccessImpl dataAccess;
	
	public CheckoutModel() {
		dataAccess = new DataAccessImpl();
	}
	
	public int InsertCheckout(String memberId, String bookId, String days ) {
		
		
		//System.out.println(memberId + " " + bookId);
		return dataAccess.inserTable("insert into CheckoutEntity values ('"+memberId+"',"+bookId+",date('now') , date('now','+"+days+" day'))");
		//return 0;
	}
	
	
	public String Checkmember(String memId) {
		return dataAccess.checkTable("select count(*) from Member where id = '"+memId+"'");
	}
	
	
	public String Checkbook(String bookId) {
		return dataAccess.checkTable("select count(*) from BookCopy where id = '"+bookId+"' and availability is true");
	}
	
	
	public int decreaseBook (String bookId) {
		return dataAccess.inserTable("update BookCopy set availability = false  where id = " + bookId);
	}
	
	
	public String getMaxDays (String bookId) {
		return dataAccess.checkTable("select maxBorrowDays from BookInfor i, BookCopy c where i.isbn = c.book_id and c.id = " + bookId);
	}
	
	
}
