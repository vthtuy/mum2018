package mpp.entity;

import mpp.Constances;

public class EmployeeModel extends UserModel {
	
	private String password;
	
	private Constances.Role roleEnum;
	
	private String role;

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public Constances.Role getRoleEnum() {
		return roleEnum;
	}

	public void setRoleEnum(Constances.Role roleEnum) {
		this.roleEnum = roleEnum;
	}
}
