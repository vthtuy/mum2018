package mpp.entity;

public class BookCopies {
		private int id;
		private Books book;
		private boolean availabilty;
		public BookCopies(Books book) {
			// TODO Auto-generated constructor stub
			this.book=book;
			this.availabilty=true;
		}
		public Books getBook() {
			return book;
		}
		public int getId() {
			return id;
		}
		public void setId(int id) {
			this.id = id;
		}
		public void setBook(Books book) {
			this.book = book;
		}
		public boolean isAvailabilty() {
			return availabilty;
		}
		public void setAvailabilty(boolean availabilty) {
			this.availabilty = availabilty;
		}
		public BookCopies() {
			// do nothing
		}
}
