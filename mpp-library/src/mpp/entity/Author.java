package mpp.entity;


public class Author  extends UserModel{
		private String bio;

		public String getBio() {
			return bio;
		}

		public void setBio(String bio) {
			this.bio = bio;
		} 
		public Author(String id,String first,String last){
			this.setId(id);
			this.setFirstName(first);
			this.setLastName(last);
		}
		
		public Author() {
			// do nothing
		}
	
}
